import 'normalize.css/normalize.css'
import './styles/main.scss'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import AppRouter from './routers/AppRouter'
import configureStore from './store/configureStore'
import { saveState } from './helpers/localStorage'
import throttle from 'lodash/throttle'

const store = configureStore()

let hasRendered = false

const renderApp = () => {
  if (!hasRendered) {
    render(jsx, document.getElementById('root'))
    hasRendered = true
  }
}

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
)

renderApp()
// throttle(lodash) permet d'éviter d'utiliser lafonction trop souvent(JSON.Str)
// est couteuse.
store.subscribe(throttle(() => {
  saveState({
    // On choisi les states à save dans localStorage(tout sauf UI state)
    articles: store.getState().articles,
    user: store.getState().user,
    adminArticleCreation: store.getState().adminArticleCreation,
    creationAccount: store.getState().creationAccount,
    dashboard: store.getState().dashboard,
    usersList: store.getState().usersList,
    filters: store.getState().filters
  })
}, 1000))
