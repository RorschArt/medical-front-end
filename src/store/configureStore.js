import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import articlesReducer from '../reducers/articlesReducer'
import articleAdminReducer from '../reducers/admin/articleAdminReducer'
import accountCreationReducer from '../reducers/accountCreationReducer'
import authReducer from '../reducers/authReducer'
import userListReducer from '../reducers/usersListReducer'
import dashboardReducer from '../reducers/user/dashboardReducer'
import mailBoxReducer from '../reducers/user/mailBoxReducer'
import filtersReducer from '../reducers/filtersReducer'
import { loadState } from '../helpers/localStorage'

// Permettre d'utiliser redux devtool et les middlewares sur la DB
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const persistedState = loadState()

export default () => {
  const store = createStore(
    combineReducers({
      articles: articlesReducer,
      user: authReducer,
      adminArticleCreation: articleAdminReducer,
      creationAccount: accountCreationReducer,
      dashboard: dashboardReducer,
      usersList: userListReducer,
      messagesSpace: mailBoxReducer,
      filters: filtersReducer
    }),
    persistedState,
    composeEnhancers(applyMiddleware(thunk))
  )

  return store
}
