import axios from 'axios'
const database = process.env.MONGODB_DATABASE_URL

const instance = axios.create({
  baseURL: `${database}`,
  headers: {'X-Custom-Header': 'foobar'}
});


instance.interceptors.request.use((config) => {
  const stateStr = localStorage.getItem('state')
  const token = JSON.parse(stateStr).user.token

  if (token) {
    config.headers.authorization = `Bearer ${token}`
    config.headers.contentType = 'application/json'
  }

  return config
})

  export default instance
