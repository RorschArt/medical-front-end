import React from 'react'
import { Switch, Router } from 'react-router-dom'
import PublicRoute from './PublicRoute'
import PrivateRoute from './PrivateRoute'
import UserDashboardPage from '../containers/UserDashboardPage'
import UserArticlesPage from '../containers/UserArticlesPage'
import UserArticleReadPage from '../containers/UserArticleReadPage'
import UserMainPage from '../containers/UserMainPage'
import UserSettingsPage from '../containers/UserSettingsPage'
import AboutPage from '../containers/AboutPage'
import UserMailsPage from '../containers/UserMailsPage'
import UserCreateMailPage from '../containers/UserCreateMailPage'
import UserAdminInfoPage from '../containers/UserAdminInfoPage'
import UserMailsSavedPage from '../containers/UserMailsSavedPage'
import UserMailsSendedPage from '../containers/UserMailsSendedPage'
import MainPage from '../containers/MainPage'
import ArticlesPage from '../containers/ArticlesPage'
import AdminCreateArticlePage from '../containers/AdminCreateArticlePage'
import AdminSuccefullCreationPage from '../containers/AdminSuccefullCreationPage'
import AdminDashboardPage from '../containers/AdminDashboardPage'
import AdminSettingsPage from '../containers/AdminSettingsPage'
import AdminMailBox from '../containers/AdminMailBox'
import AdminEditOneArticlePage from '../containers/AdminEditOneArticlePage'
import AdminRoute from './AdminRoute'
import history from './history'
import { ToastContainer, Flip } from 'react-toastify'

const AppRouter = () => (
  <Router history={history}>
    <div>
      <ToastContainer
        autoClose={1000}
        hideProgressBar
        newestOnTop
        closeOnClick
        rtl={false}
        draggable
        pauseOnHover
        transition={Flip}
      />
      <Switch>
        <PrivateRoute exact path='/dashboard' component={UserDashboardPage} />
        <PrivateRoute exact path='/user/articles' component={UserArticlesPage} />
        <PrivateRoute exact path='/user/article/read' component={UserArticleReadPage} />
        <PrivateRoute exact path='/dashboard/settings' component={UserSettingsPage} />
        <PrivateRoute exact path='/dashboard/adminList' component={UserAdminInfoPage} />
        <PrivateRoute exact path='/user' component={UserMainPage} />
        <PrivateRoute exact path='/dashboard/mailbox/received' component={UserMailsPage} />
        <PrivateRoute exact path='/dashboard/mailbox/create' component={UserCreateMailPage} />
        <PrivateRoute exact path='/dashboard/mailbox/saved' component={UserMailsSavedPage} />
        <PrivateRoute exact path='/dashboard/mailbox/sended' component={UserMailsSendedPage} />
        <AdminRoute exact path='/dashboard/admin/createArticle' component={AdminCreateArticlePage} />
        <AdminRoute exact path='/admin/editArticle/:id' component={AdminEditOneArticlePage} />
        <AdminRoute exact path='/dashboard/admin/mailBox' component={AdminMailBox} />
        <AdminRoute exact path='/dashboard/admin' component={AdminDashboardPage} />
        <AdminRoute exact path='/dashboard/admin/settings' component={AdminSettingsPage} />
        <AdminRoute exact path='/dashboard/admin/CreateSucceed' component={AdminSuccefullCreationPage} />
        <PublicRoute exact path='/about' component={AboutPage} />
        <PublicRoute path='/articles' component={ArticlesPage} />
        <PublicRoute path='/' component={MainPage} />
      </Switch>
    </div>
  </Router>
)

export default AppRouter
