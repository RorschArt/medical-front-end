import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import Header from '../components/layout/Header'
import Footer from '../components/layout/Footer'
import { startLogout } from '../actions/loginActions'

const linksHeaderPrivate = [
  { url: '/dashboard', name: 'Mon Espace' },
  { url: '/dashboard/admin/createArticle', name: 'Créer un Article' },
  { url: '/dashboard/admin/settings', name: 'Paramètres' },
  { url: '/dashboard/admin/mailBox', name: 'Mailbox' }
]

class AdminRoute extends Component {

  render() {
    const {
      user,
      disconnect,
      component: Component,
      ...rest
    } = this.props

    return (
      <Route
        {...rest}
        component={({...rest}) => {
          if (user.isAdmin && user.isAuthenticated) {
            return (
              <div>
                <div className=''>
                <Header
                  admin
                  {...rest}
                  handleDisconnect={disconnect}
                  needUnreadCounter
                  needButtonDisconnect={user.isAuthenticated}
                  links={linksHeaderPrivate}
                  />
                  <Component {...rest}{...this.props} />
                </div>
                <Footer
                  admin
                />
              </div>
            )
            // Si ce n'est pas un user connecté le Private route se chargera de la
            // redirection vers le Public Route
          } else if (!user.isAdmin) {
            return <Redirect to='/dashboard'/>
          }
        }
      }
    />
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    disconnect: () => dispatch(startLogout()),
  }
}

const mapStateToProps = (state) => ({
  user: state.user
})

export default connect(mapStateToProps, mapDispatchToProps)(AdminRoute)
