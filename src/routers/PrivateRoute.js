import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import Header from '../components/layout/Header'
import Footer from '../components/layout/Footer'
import { startLogout } from '../actions/loginActions'
import { startFetchMessageReceived } from '../actions/users/mails/mailActions'
import { getAllAdmins } from '../actions/admin/adminActions'
import 'react-toastify/dist/ReactToastify.css'
import { getAllArticles } from '../actions/articlesActions'

const linksHeaderPrivate = [
  { url: '/dashboard', name: 'Mon Espace' },
  { url: '/user', name: 'Accueil' },
  { url: '/user/articles', name: 'Informations' },
  { url: '/dashboard/settings', name: 'Paramètres' },
]

class PrivateRoute extends Component {

  componentDidMount() {
    this.props.fetchAdmins()
    this.props.fetchAllArticles()
  }

  render () {
    const {
      user,
      dashboard,
      disconnect,
      component: Component,
      ...rest
    } = this.props

    return (
      <Route
        {...rest}
        component={({...rest}) => {
          if (user.isAuthenticated && !user.isAdmin) { // un connecté non admin
            return (
              <div className='mh-100'>
                <Header
                  user={this.props.user}
                  dashboard={this.props.dashboard}
                  {...rest}
                  userHeader
                  needUnreadCounter
                  handleDisconnect={disconnect}
                  needButtonDisconnect={user.isAuthenticated}
                  links={linksHeaderPrivate}
                />
                <div className='pt-5 mt-5' id='root-comp'>
                <Component {...rest} {...this.props } />
                </div>
                <Footer />
              </div>
            )
          }
          else if (user.isAuthenticated && user.isAdmin) { // est admin
            return <Redirect to='/dashboard/admin' />
          }
          else if (!user.isAuthenticated) { // est un invité
            return <Redirect to='/' />
          }
        }}
      />
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    disconnect: () => dispatch(startLogout()),
    fetchMessageReceived: () => dispatch(startFetchMessageReceived()),
    fetchAdmins: () => dispatch(getAllAdmins()),
    fetchAllArticles: () => dispatch(getAllArticles())
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  dashboard: state.dashboard
})

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute)
