import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import Header from '../components/layout/Header'
import Footer from '../components/layout/Footer'
import { fetchArticlesIfNeeded } from '../actions/articlesActions'
import history from './history'
import { getAllArticles } from '../actions/articlesActions'


const linksHeaderPublic = [
  { url: '/', name: 'Accueil' },
  { url: '/articles', name: 'Informations' }
]

class PublicRoute extends Component {

  componentDidMount() {
    // this.props.fetchAdmins()
    this.props.fetchAllArticles()
  }

  render () {
    const {
      isAuthenticated,
      isAdmin,
      isLoginPending,
      component: Component,
      isCreationPending,
      ...rest
    } = this.props

    return (
      <Route
        {...rest}
        component={(props) => {
          if (isAuthenticated) {
            return <Redirect to='/user' />
          }

          else if (!isAuthenticated) {
            return (
              <div className='mh-100 '>
                <Header
                  history={history}
                  links={linksHeaderPublic}
                />
                <div className='pt-2 mt-5 container' id='root-comp'>
                {
                  isLoginPending &&
                    <div style={{width: 20 +'rem', height:15 + 'rem'}}>
                    <div className="text-center">
                      <div className="spinner-border" role="status">
                      </div>
                    </div>
                    </div>
                }
                <Component {...props} />
                </div>
                <Footer />
              </div>
            )
          }
        }}
      />
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadArticles: () => dispatch(fetchArticlesIfNeeded()),
    fetchAllArticles: () => dispatch(getAllArticles())
  }
}

const mapStateToProps = (state) => ({
    isAuthenticated: state.user.isAuthenticated,
    isAdmin: state.user.isAdmin,
    isLoginPending: state.user.isLoginPending,
    isCreationPending: state.creationAccount.isCreationPending,
  })

export default connect(mapStateToProps, mapDispatchToProps)(PublicRoute)
