import {
  SET_USER_INFOS,
  SET_LOGIN_PENDING,
  SET_IS_ADMIN,
  SET_IS_AUTHENTICATED,
  SET_LOGIN_HAS_FAILED,
  SET_LOGOUT_PENDING,
  SET_LOGOUT_HAS_FAILED,
  SET_IS_NOT_AUTHENTICATED
} from '../actions/loginActions'

import {
  SET_DELETE_ACCOUNT_PENDING,
  SET_DELETE_ACCOUNT,
  SET_DELETE_ACCOUNT_INVALIDATE
} from '../actions/users/accountActions'

const accountDefaultState = {
  data: {},
  token: '',
  isAuthenticated: false,
  isLoginPending: false,
  isLogoutPending: false,
  isAdmin: false,
  loginHasFailed: false,
  logoutHasFailed: false,
  lastConnection: null,
  lastDisconnection: null,
  deleteAccountPending: false,
  deleteAccountInvalidate: false
}
export default (state = accountDefaultState, action) => {
  switch (action.type) {
    case SET_LOGIN_PENDING:
      return ({
        ...state,
        isLoginPending: true,
        loginHasFailed: false
      })
    case SET_USER_INFOS:
      return ({
        ...state,
        isLoginPending: false,
        isLogoutPending: false,
        isAuthenticated: true,
        data: action.data,
        token: action.token,
        lastDisconnection: action.lastConnection
      })
    case SET_IS_AUTHENTICATED:
      return ({
        ...state,
        isAdmin: false,
        isLoginPending: false,
        loginHasFailed: false,
        isLogoutPending: false,
        isAuthenticated: true,
        lastConnection: action.lastConnection
      })
    case SET_IS_NOT_AUTHENTICATED:
      return ({
        ...state,
        isAdmin: false,
        isLoginPending: false,
        loginHasFailed: false,
        isLogoutPending: false,
        isAuthenticated: false,
        token: '',
        data: [],
        lastDisconnection: action.lastDisconnection
      })
    case SET_IS_ADMIN:
      return ({
        ...state,
        isAdmin: action.isAdmin
      })
    case SET_LOGIN_HAS_FAILED:
      return ({
        ...state,
        isAdmin: false,
        isLoginPending: false,
        loginHasFailed: true
      })
    case SET_LOGOUT_PENDING:
      return ({
        ...state,
        isLogoutPending: action.isLogoutPending
      })
    case SET_LOGOUT_HAS_FAILED:
      return ({
        ...state,
        logoutHasFailed: action.logoutHasFailed,
        isLogoutPending: false
      })
      case SET_DELETE_ACCOUNT_PENDING:
        return ({
          ...state,
          deleteAccountPending: true
        })
      case SET_DELETE_ACCOUNT:
        return ({
          ...accountDefaultState
        })
      case SET_DELETE_ACCOUNT_INVALIDATE:
        return ({
          ...state,
          deleteAccountInvalidate: true
        })


    default:
      return state
  }
}
