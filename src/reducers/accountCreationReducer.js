import {
  SET_REQUEST_ACCOUNT_CREATION,
  SET_INFOS_ACCOUNT_CREATION,
  SET_ACCOUNT_CREATION_FAILED,
  SET_ACCOUNT_CREATION_SUCCEED
} from '../actions/accountCreationActions'

export default (state = {
  email: '',
  password: '',
  pseudoName: '',
  isCreationPending: false,
  isCreationFailed: false
}, action) => {
  switch (action.type) {
    case SET_REQUEST_ACCOUNT_CREATION:
      return ({
        ...state,
        isCreationPending: true
      })
    case SET_ACCOUNT_CREATION_SUCCEED:
      return ({
        ...state,
        isCreationPending: false,
        isCreationFailed: false
      })
    case SET_INFOS_ACCOUNT_CREATION:
      return ({
        ...state,
        email: action.email,
        password: action.password,
        pseudoName: action.pseudoName
      })
    case SET_ACCOUNT_CREATION_FAILED:
      return ({
        ...state,
        isCreationPending: false,
        isCreationFailed: true,
      })
    default:
      return state
  }
}
