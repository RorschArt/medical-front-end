import {
  SET_DISPLAY_AVATAR_FILTER,
  SET_DISPLAY_USERS_LIST_FILTER,
  SET_DISPLAY_SEND_MSG_COMPONENT,
  SET_DISPLAY_SAVE_MSG_COMPONENT,
  SET_DISPLAY_RECEIVE_MSG_COMPONENT,
  SET_PSEUDONAME_FILTER_USERS_LIST,
  SET_MSG_RECEIVED_FOCUSED,
  SET_MSG_SAVED_FOCUSED,
  SET_MSG_SENDED_FOCUSED,
  SET_MSG_SAVED_DELETED_FOCUSED,
  SET_MSG_RECEIVED_DELETED_FOCUSED,
  SET_MSG_SENDED_DELETED_FOCUSED,
  SET_USER_ID_FOCUSED,
  SET_DISPLAY_USER_PREVIEW,
  SET_USER_INFOS_FOCUSED,
  SET_DELETE_INFOS_USER_FOCUSED,
  SET_DELETE_ID_USER_FOCUSED,
  SET_ADMIN_ARTICLES_EXPANDED,
  SET_INFOS_ARTICLE
} from '../actions/filterActions'

const defaultState = {
  displayAvatar: false,
  displayUsersList: false,
  displayUserPreview: false,
  pseudoNameFilter: null,
  articleFocused: null,
  sendEmailCompVisible: false,
  saveEmailCompVisible: false,
  receiveEmailCompVisible: false,
  msgReceivedFocused: {},
  msgSavedFocused: {},
  msgSendedFocused: {},
  msgReceivedDeleted: {},
  msgSavedDeleted: {},
  msgSendedDeleted: {},
  userIdFocused: null,
  userFocused: null,
  adminArticlesIsExpanded: false
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case SET_PSEUDONAME_FILTER_USERS_LIST:
      return ({
        ...state,
        pseudoNameFilter: action.pseudoName
      })
    case SET_DISPLAY_AVATAR_FILTER:
      return ({
        ...state,
        displayAvatar: action.displayAvatar
      })
    case SET_DISPLAY_USERS_LIST_FILTER:
      return ({
        ...state,
        displayUsersList: action.displayUsersList
      })
    case SET_DISPLAY_USER_PREVIEW:
      return ({
        ...state,
        displayUserPreview: action.UserPreviewVisible
      })
    case SET_DISPLAY_SEND_MSG_COMPONENT:
      return ({
        ...state,
        sendEmailCompVisible: action.sendEmailCompVisible
      })
    case SET_DISPLAY_SAVE_MSG_COMPONENT:
      return ({
        ...state,
        saveEmailCompVisible: action.saveEmailCompVisible
      })
    case SET_DISPLAY_RECEIVE_MSG_COMPONENT:
      return ({
        ...state,
        receiveEmailCompVisible: action.receiveEmailCompVisible
      })
    case SET_MSG_SENDED_FOCUSED:
      return ({
        ...state,
        msgSendedFocused: action.msg
      })
    case SET_MSG_SAVED_FOCUSED:
      return ({
        ...state,
        msgSavedFocused: action.msg
      })
    case SET_MSG_RECEIVED_FOCUSED:
      return ({
        ...state,
        msgReceivedFocused: action.msg
      })
    case SET_MSG_SAVED_DELETED_FOCUSED:
      return ({
        ...state,
        msgSavedDeleted: action.msg
      })
    case SET_MSG_RECEIVED_DELETED_FOCUSED:
      return ({
        ...state,
        msgReceivedDeleted: action.msg
      })
    case SET_MSG_SENDED_DELETED_FOCUSED:
      return ({
        ...state,
        msgSendedDeleted: action.msg
      })
    case SET_USER_ID_FOCUSED:
      return ({
        ...state,
        userIdFocused: action.userId
      })
    case SET_USER_INFOS_FOCUSED:
      return ({
        ...state,
        userFocused: action.user
      })
    case SET_DELETE_INFOS_USER_FOCUSED:
      return ({
        ...state,
        userFocused: null
      })
    case SET_DELETE_ID_USER_FOCUSED:
      return ({
        ...state,
        userIdFocused: null
      })
    case SET_ADMIN_ARTICLES_EXPANDED:
      return ({
        ...state,
        adminArticlesIsExpanded: action.isExpanded
      })
      case SET_INFOS_ARTICLE:
        return {
          ...state,
          articleFocused: action.article
        }
    default:
      return state
  }
}
