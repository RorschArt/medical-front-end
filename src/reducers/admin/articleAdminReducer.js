import {
  SET_INVALIDATE_UPLOAD_ARTICLE,
  SET_PENDING_UPLOAD_ARTICLE,
  SET_SUCCESS_UPLOAD_ARTICLE,
  SET_INVALIDATE_UPLOAD_IMAGE_ARTICLE,
  SAVE_ID_IMAGE_ARTICLE_CREATED_IN_STORE,
  SET_SUCCESS_UPLOAD_IMAGE_ARTICLE,
  SET_PENDING_UPLOAD_IMAGE_ARTICLE,
  SET_INVALIDATE_ID_IMAGE_ARTICLE_CREATED_IN_STORE,

} from '../../actions/admin/createArticleAction'
import {
  SET_ID_ARTICLE_FOCUSED,
  SET_SUCCESS_DELETING_ARTICLE,
  SET_INVALIDATE_DELETING_ARTICLE,
  SET_PENDING_DELETING_ARTICLE
} from '../../actions/admin/editArticlesActions'

export default (state = {
  content: {
    titleArticle: null,
    subTitleArticle: null,
    textBodyImage: null,
    idImageArticleCreated: null,
    imageUrl: null
  },
  deletinArticlePending: false,
  didDeletingArticleInvalidate: false,
  articleDeleted: null,
  didSaveIdImageInvalidate: false,
  didContentArticleInvalidate: false,
  imageUploaded: false,
  idArticleFocused: null,
  didImageUploadInvalidate: false,
  contentArticleIsUploading: false,
  imageArticleisUploading: false
}, action) => {
  switch (action.type) {
    case SET_PENDING_UPLOAD_ARTICLE:
      return ({
        ...state,
        contentArticleIsUploading: true,
        didContentArticleInvalidate: false,
        didImageUploadInvalidate: false,
        error: undefined
      })
      case SET_SUCCESS_UPLOAD_ARTICLE:
        return ({
          ...state,
          content: action.article,
          contentArticleIsUploading: false,
          didContentArticleInvalidate: false,
          didImageUploadInvalidate: false,
          error: undefined
        })
        case SET_INVALIDATE_UPLOAD_ARTICLE:
          return ({
            ...state,
            contentArticleIsUploading: false,
            didContentArticleInvalidate: true,
            didImageUploadInvalidate: false,
            error: 'INVALIDATE_UPLOAD_ARTICLE'
          })
      case SET_PENDING_UPLOAD_IMAGE_ARTICLE:
      return ({
        ...state,
        imageArticleisUploading: true,
        imageUploaded: false
      })
      case SET_SUCCESS_UPLOAD_IMAGE_ARTICLE:
      return ({
        ...state,
        imageArticleisUploading: false,
        imageUploaded: true
      })
    case SET_INVALIDATE_UPLOAD_IMAGE_ARTICLE:
      return ({
        ...state,
        imageArticleIsUploading: false,
        didImageUploadInvalidate: true,
        error: 'INVALIDATE_UPLOAD_IMAGE_ARTICLE'
      })
      case SAVE_ID_IMAGE_ARTICLE_CREATED_IN_STORE:
        return ({
          ...state,
          content :{
            idImageArticleCreated: action.idImageArticleCreated
          },
          didImageUploadInvalidate: false,
        })
    case SET_INVALIDATE_ID_IMAGE_ARTICLE_CREATED_IN_STORE:
      return ({
        ...state,
        didSaveIdImageInvalidate: true,
        didImageUploadInvalidate: true,
        error: 'INVALIDATE_UPLOAD_IMAGE_ARTICLE'
      })
    case SET_ID_ARTICLE_FOCUSED:
      return ({
        ...state,
        idArticleFocused: action.idArticle
      })
    case SET_SUCCESS_DELETING_ARTICLE:
      return ({
        ...state,
        deletinArticlePending: false,
        articleDeleted: action.article,
        didDeletingArticleInvalidate: false
      })
    case SET_INVALIDATE_DELETING_ARTICLE:
      return ({
        ...state,
      })
    case SET_PENDING_DELETING_ARTICLE:
      return ({
        ...state,
        deletinArticlePending: true,
        didDeletingArticleInvalidate: false
      })

    default:
      return state
  }
}
