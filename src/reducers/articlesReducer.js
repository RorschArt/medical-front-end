import {
  SET_INFOS_ARTICLES_RECEIVED,
  SET_INVALIDATE_ARTICLES,
  SET_REQUEST_ARTICLES
} from '../actions/articlesActions'

export default (state = {
  items: [],
  isPending: false,
  didInvalidate: false,
  lastUpdate: null,
  error: false
}, action) => {
  switch (action.type) {
    case SET_REQUEST_ARTICLES:
      return ({
        ...state,
        isPending: true,
        didInvalidate: false,
        error: false
      })
    case SET_INFOS_ARTICLES_RECEIVED:
      return ({
        ...state,
        items: action.articles,
        isPending: false,
        didInvalidate: false,
        error: false,
        lastUpdate: action.lastUpdate
      })
    case SET_INVALIDATE_ARTICLES:
      return {
        ...state,
        isPending: false,
        didInvalidate: true,
        error: 'INVALIDATE_ARTICLES'
      }
    default:
      return state
  }
}
