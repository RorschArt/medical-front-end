import {
  SET_REQUEST_USERS_LIST,
  SET_INVALIDATE_USERS_LIST,
  SET_INFOS_USERS_LIST_RECEIVED
} from '../actions/usersListActions'

export default (state = {
  resultItem: null,
  isFetching: false,
  didInvalidate: false,
}, action) => {
  switch (action.type) {
    case SET_REQUEST_USERS_LIST:
      return ({
        ...state,
        didInvalidate: false,
        isFetching: true
      })
    case SET_INFOS_USERS_LIST_RECEIVED:
      return ({
        ...state,
        isFetching: false,
        resultItem: action.usersList,
        didInvalidate: false
      })
    case SET_INVALIDATE_USERS_LIST:
      return ({
        ...state,
        didInvalidate: true,
        isFetching: false
      })
    default:
      return state
  }
}
