//TODO: Séparer ce Reducer en plusieurs parties et utiliser CombineReducer
import {
  SET_PENDING_SEND_MSG,
  SET_SUCCESS_SEND_MSG,
  SET_INVALIDATE_SEND_MSG,
  SET_PENDING_SAVING_MSG,
  SET_SUCCESS_SAVE_MSG,
  SET_INVALIDATE_SAVE_MSG,
  SET_PENDING_FETCH_RECEIVED_MSG,
  SET_SUCCESS_FETCH_RECEIVED_MSG,
  SET_INVALIDATE_FETCH_RECEIVED_MSG,
  SET_PENDING_FETCH_SAVED_MSG,
  SET_SUCCESS_FETCH_SAVED_MSG,
  SET_PENDING_DELETE_RECEIVED_MSG,
  SET_SUCCESS_MSG_RECEIVED_COUNTER,
  SET_INVALIDATE_DELETE_RECEIVED_MSG,
  SET_SUCCESS_DELETE_RECEIVED_MSG,
  SET_INVALIDATE_FETCH_SAVED_MSG,
  SET_PENDING_FETCH_SENDED_MSG,
  SET_SUCCESS_FETCH_SENDED_MSG,
  SET_INVALIDATE_FETCH_SENDED_MSG,
  SET_PENDING_DELETE_SAVED_MSG,
  SET_INVALIDATE_DELETE_SAVED_MSG,
  SET_SUCCESS_DELETE_SAVED_MSG,
  SET_PENDING_MSG_UNREAD_COUNTER,
  SET_SUCCESS_MSG_UNREAD_COUNTER,
  SET_INVALIDATE_MSG_UNREAD_COUNTER,
  SET_SUCCESS_MSG_SENDED_COUNTER,
  SET_SUCCESS_MSG_SAVED_COUNTER,
  SET_SUCCESS_MSG_UNREAD,
  SET_INVALIDATE_MSG_UNREAD,
  SET_PENDING_MSG_UNREAD,
  SET_PENDING_EDIT_MSG_SAVED,
  SET_SUCCESS_EDIT_MSG_SAVED,
  SET_INVALIDATE_EDIT_MSG_SAVED,
  SET_PENDING_DELETE_ALL_RECEIVED_MSG,
  SET_SUCCESS_DELETE_ALL_RECEIVED_MSG,
  SET_INVALIDATE_DELETE_ALL_RECEIVED_MSG,
  SET_SUCCESS_DELETE_ALL_SAVED_MSG,
  SET_SUCCESS_DELETE_SENDED_MSG,
  SET_INVALIDATE_DELETE_ALL_SAVED_MSG,
  SET_PENDING_DELETE_ALL_SAVED_MSG,
  SET_PENDING_MSG_RECEIVED_COUNTER,
  SET_SUCCESS_DELETE_ALL_SENDED_MSG,
  SET_INVALIDATE_DELETE_ALL_SENDED_MSG,
  SET_PENDING_DELETE_ALL_SENDED_MSG,
  SET_CONTENT_MSG_TO_SEND
} from '../../actions/users/mails/mailActions'

const defaultState = {
  sendingPendingMsg: false,
  didSendingMsgInvalidate: false,
  savingPendingMsg: false,
  didSavingMsgInvalidate: false,
  msgReceivedListItems: [],
  msgReceivedFetching: false,
  msgReceivedInvalidate: false,
  msgSavedListItems: [],
  msgSavedFetching: false,
  msgSavedInvalidate: false,
  msgSendedListItems: [],
  msgSendedFetching: false,
  msgSendedInvalidate: false,
  receivedMsgDeletePending: false,
  didDeletingReceivedMsgInvalidate: false,
  savedMsgDeletePending: false,
  didDeletingSavedMsgInvalidate: false,
  unreadMsgCounter: undefined,
  counterUnreadMsgPending: false,
  counterUnreadMsgInvalidate: false,
  sendedCounter: 0,
  savedCounter: 0,
  receivedCounter: undefined,
  unreadMsgPending: false,
  unreadMsgInvalidate: false,
  unreadMsgListItems: [],
  msgReceivedCounter: 0,
  editMsgSavedPending: false,
  didEditMsgSavedInvalidate: false,
  deletingAllMessageReceivedPending: false,
  didDeleteAllMessageReceivedInvalidate: false,
  deletingAllMessageSavedPending: false,
  didDeleteAllMessageSavedInvalidate: false,
  deletingAllMessageSendedPending: false,
  didDeleteAllMessageSendedInvalidate: false,
  contentMsgToSend: null
}
export default (state = defaultState, action) => {
  switch (action.type) {
    case SET_PENDING_SEND_MSG:
      return ({
        ...state,
        sendingPendingMsg: true,
        didSendingMsgInvalidate: false
      })
    case SET_SUCCESS_SEND_MSG:
      return ({
        ...state,
        sendingPendingMsg: false,
        didSendingMsgInvalidate: false
      })
    case SET_INVALIDATE_SEND_MSG:
      return ({
        ...state,
        sendingPendingMsg: false,
        didSendingMsgInvalidate: true
      })
    case SET_PENDING_SAVING_MSG:
      return ({
        ...state,
        savingPendingMsg: true,
        didSavingMsgInvalidate: false
      })
    case SET_SUCCESS_SAVE_MSG:
      return ({
        ...state,
        savingPendingMsg: false,
        didSavingMsgInvalidate: true
      })
    case SET_INVALIDATE_SAVE_MSG:
      return ({
        ...state,
        savingPendingMsg: false,
        didSavingMsgInvalidate: false
      })
    case SET_PENDING_FETCH_RECEIVED_MSG:
      return ({
        ...state,
        msgReceivedFetching: true,
        msgReceivedInvalidate: false
      })
    case SET_SUCCESS_FETCH_RECEIVED_MSG:
      return ({
        ...state,
        msgReceivedCounter: action.messages.length,
        msgReceivedListItems: action.messages,
        msgReceivedFetching: false,
      })
    case SET_INVALIDATE_FETCH_RECEIVED_MSG:
      return ({
        ...state,
        msgReceivedInvalidate: true,
        msgReceivedFetching: false
      })
    case SET_PENDING_FETCH_SAVED_MSG:
      return ({
        ...state,
        msgSavedFetching: true,
        msgSavedInvalidate: false
      })
    case SET_SUCCESS_FETCH_SAVED_MSG:
      return ({
        ...state,
        msgSavedFetching: false,
        msgSavedListItems: action.messages
      })
    case SET_INVALIDATE_FETCH_SAVED_MSG:
      return ({
        ...state,
        msgSavedFetching: false,
        msgSavedInvalidate: true
      })
    case SET_PENDING_FETCH_SENDED_MSG:
      return ({
        ...state,
        msgSendedFetching: true,
        msgSendedInvalidate: false
      })
    case SET_SUCCESS_FETCH_SENDED_MSG:
      return ({
        ...state,
        msgSendedFetching: false,
        msgSendedListItems: action.messages
      })
    case SET_INVALIDATE_FETCH_SENDED_MSG:
      return ({
        ...state,
        msgSendedFetching: false
      })
    case SET_PENDING_DELETE_SAVED_MSG:
      return ({
        ...state,
        savedMsgDeletePending: true,
        didDeletingSavedMsgInvalidate: false
      })
    case SET_SUCCESS_DELETE_SAVED_MSG:
      return ({
        ...state,
        savedMsgDeletePending: false,
        didDeletingSavedMsgInvalidate: false
      })
    case SET_INVALIDATE_DELETE_SAVED_MSG:
      return ({
        ...state,
        savedMsgDeletePending: false,
        didDeletingSavedMsgInvalidate: true
      })
    case SET_PENDING_MSG_UNREAD_COUNTER:
      return ({
        ...state,
        counterUnreadMsgPending: true,
        counterUnreadMsgInvalidate: false
      })
    case SET_SUCCESS_MSG_UNREAD_COUNTER:
      return ({
        ...state,
        counterUnreadMsgPending: false,
        counterUnreadMsgInvalidate: false,
        unreadMsgCounter: action.msgCounter
      })
    case SET_INVALIDATE_MSG_UNREAD_COUNTER:
      return ({
        ...state,
        counterUnreadMsgPending: false,
        counterUnreadMsgInvalidate: true
      })
    case SET_PENDING_MSG_UNREAD:
      return ({
        ...state,
        unreadMsgPending: true,
        unreadMsgInvalidate: false
      })
    case SET_SUCCESS_MSG_UNREAD:
      return ({
        ...state,
        unreadMsgPending: false,
        unreadMsgInvalidate: false,
        unreadMsgListItems: action.msg
      })
    case SET_INVALIDATE_MSG_UNREAD:
      return ({
        ...state,
        unreadMsgPending: false,
        unreadMsgInvalidate: true
      })
    case SET_PENDING_EDIT_MSG_SAVED:
      return ({
        ...state,
        editMsgSavedPending: true,
        didEditMsgSavedInvalidate: false
      })
    case SET_SUCCESS_EDIT_MSG_SAVED:
      return ({
        ...state,
        editMsgSavedPending: false,
      didEditMsgSavedInvalidate: false
      })
    case SET_INVALIDATE_EDIT_MSG_SAVED:
      return ({
        ...state,
        editMsgSavedPending: false,
        didEditMsgSavedInvalidate:true
      })
    case SET_PENDING_DELETE_ALL_RECEIVED_MSG:
      return ({
        ...state,
        deletingAllMessageReceivedPending: true,
        didDeleteAllMessageReceivedInvalidate: false
      })
    case SET_SUCCESS_DELETE_ALL_RECEIVED_MSG:
      return ({
        ...state,
        deletingAllMessageReceivedPending: false,
        didDeleteAllMessageReceivedInvalidate: false
      })
    case SET_INVALIDATE_DELETE_ALL_RECEIVED_MSG:
      return ({
        ...state,
        deletingAllMessageReceivedPending: false,
        didDeleteAllMessageReceivedInvalidate: true
      })
    case SET_PENDING_DELETE_ALL_SAVED_MSG:
      return ({
        ...state,
        deletingAllMessageSavedPending: true,
        didDeleteAllMessageSavedInvalidate: false
      })
    case SET_SUCCESS_DELETE_ALL_SAVED_MSG:
      return ({
        ...state,
        deletingAllMessageSavedPending: false,
        didDeleteAllMessageSavedInvalidate: false
      })
    case SET_INVALIDATE_DELETE_ALL_SAVED_MSG:
      return ({
        ...state,
        deletingAllMessageSavedPending: false,
        didDeleteAllMessageSavedInvalidate: true
      })
    case SET_PENDING_DELETE_ALL_SENDED_MSG:
      return ({
        ...state,
        deletingAllMessageSendedPending: true,
        didDeleteAllMessageSendedInvalidate: false
      })
    case SET_SUCCESS_DELETE_ALL_SENDED_MSG:
      return ({
        ...state,
        deletingAllMessageSendedPending: false,
        didDeleteAllMessageSendedInvalidate: false
      })
    case SET_INVALIDATE_DELETE_ALL_SENDED_MSG:
      return ({
        ...state,
        deletingAllMessageSendedPending: false,
        didDeleteAllMessageSendedInvalidate: true
      })
    case SET_CONTENT_MSG_TO_SEND:
      return ({
        ...state,
        contentMsgToSend: action.content
      })
    case SET_SUCCESS_DELETE_SENDED_MSG:
      return ({
        ...state,
        msgSendedListItems: []
      })
    case SET_PENDING_DELETE_RECEIVED_MSG:
      return ({
        ...state,
        receivedMsgDeletePending: true,
        didDeletingReceivedMsgInvalidate: false
      })
    case SET_INVALIDATE_DELETE_RECEIVED_MSG:
      return ({
        ...state,
        receivedMsgDeletePending: false,
        didDeletingReceivedMsgInvalidate: false
      })
    case SET_SUCCESS_DELETE_RECEIVED_MSG:
      return ({
        ...state,
        receivedMsgDeletePending: false,
        didDeletingReceivedMsgInvalidate: true
      })
    case SET_SUCCESS_MSG_SENDED_COUNTER:
      return ({
        ...state,
        sendedCounter: action.msgCounter
        })
    case SET_SUCCESS_MSG_SAVED_COUNTER:
      return ({
        ...state,
        savedCounter: action.msgCounter
        })
    case SET_SUCCESS_MSG_RECEIVED_COUNTER:
      return ({
        ...state,
        receivedCounter: action.msgCounter,
        msgReceivedFetching: false
        })
    case SET_PENDING_MSG_RECEIVED_COUNTER:
      return ({
        ...state,
        msgReceivedFetching: true
      })
    default:
      return state
  }
}
