import {
  SET_AVATAR_UPLOAD_PENDING,
  SET_AVATAR_UPLOAD_SUCCESS,
  SET_AVATAR_UPLOAD_ERROR,
  SET_AVATAR_ID,
  SET_DELETE_DASHBOARD_PENDING,
  SET_DELETE_DASHBOARD_INFOS,
  SET_DELETE_DASHBOARD_INVALIDATE
} from '../../actions/users/dashboardAction'

import {
  SET_PASSWORD_CHECKING_PENDING,
  SET_PASSWORD_CHECKING_INVALIDATE,
  SET_PASSWORD_CHECKING_SUCCESS
} from '../../actions/loginActions'

import {
  SET_PASSWORD_RESET_PENDING,
  SET_PASSWORD_RESET_INVALIDATE,
  SET_PASSWORD_RESET_SUCCESS,
  SET_DISPLAY_MODAL_RESET_PASSWORD
} from '../../actions/users/accountActions'

import {
  SET_INFOS_ADMINS_RECEIVED,
  SET_INVALIDATE_ADMINS_LIST,
  SET_REQUEST_ADMINS_LIST,
  SET_ADMIN_INFOS_MSG
} from '../../actions/admin/adminActions'


const defaultState = {
  uploadAvatarPending: false,
  adminList: undefined,
  adminListInvalidate: false,
  adminListPending: false,
  avatarId: null,
  adminSelectedMsg: undefined,
  errorImageUpload: null,
  deleteDashboardPending: false,
  deleteDashboardDidInvalidate: false,
  passwordCheckingPending: false,
  passwordCheckingDidInvalidate: false,
  resetPasswordPending: false,
  resetPasswordDidInvalidate: false,
  displayModalResetPassword: false
}
export default (state = defaultState, action) => {
  switch (action.type) {
    case SET_AVATAR_UPLOAD_PENDING:
      return ({
        ...state,
        uploadAvatarPending: true,
        didAvatarUploadInvalidate: false
      })
    case SET_AVATAR_UPLOAD_SUCCESS:
      return ({
        ...state,
        didAvatarUploadInvalidate: false,
        uploadAvatarPending: false

      })
    case SET_AVATAR_UPLOAD_ERROR:
      return ({
        ...state,
        uploadAvatarPending: false,
        didAvatarUploadInvalidate: true,
        errorImageUpload: action.errorMessage
      })
      case SET_AVATAR_ID:
      return ({
        ...state,
        avatarId: action.avatarId
      })
      case SET_DELETE_DASHBOARD_PENDING:
        return ({
          ...state,
          deleteDashboardPending: true
        })
      case SET_DELETE_DASHBOARD_INFOS:
        return ({
          ...defaultState
        })
      case SET_DELETE_DASHBOARD_INVALIDATE:
        return ({
          ...state,
          deleteDashboardPending: false,
          deleteDidInvalidate: true
        })
      case SET_PASSWORD_CHECKING_PENDING:
        return ({
          ...state,
          passwordCheckingPending: true,
          passwordCheckingDidInvalidate: false
        })
      case SET_PASSWORD_CHECKING_INVALIDATE:
        return ({
          ...state,
          passwordCheckingPending: false,
          passwordCheckingDidInvalidate: true
        })
      case SET_PASSWORD_CHECKING_SUCCESS:
        return ({
          ...state,
          passwordCheckingPending: false,
          passwordCheckingDidInvalidate: false,
          displayModalResetPassword: false
        })
      case SET_PASSWORD_RESET_PENDING:
        return ({
          ...state,
          resetPasswordDidInvalidate: false,
          resetPasswordPending: true,
          displayModalResetPassword: true
        })
      case SET_PASSWORD_RESET_INVALIDATE:
        return ({
          ...state,
          resetPasswordDidInvalidate: true,
          resetPasswordPending: false,
          displayModalResetPassword: true
        })
      case SET_PASSWORD_RESET_SUCCESS:
        return ({
          ...state,
          resetPasswordDidInvalidate: false,
          resetPasswordPending: false,
          displayModalResetPassword: false
        })
      case SET_DISPLAY_MODAL_RESET_PASSWORD:
        return ({
          ...state,
          displayModalResetPassword: action.displayModal
        })



      case SET_INFOS_ADMINS_RECEIVED:
        return ({
          ...state,
          adminList: action.adminList,
          adminListInvalidate: false,
          adminListPending: false
        })
      case SET_INVALIDATE_ADMINS_LIST:
        return ({
          ...state,
          adminList: action.adminList,
          adminListInvalidate: true,
          adminListPending: false
        })
      case SET_REQUEST_ADMINS_LIST:
        return ({
          ...state,
          adminListInvalidate: false,
          adminListPending: true
        })
      case SET_ADMIN_INFOS_MSG:
        return ({
          ...state,
          adminSelectedMsg: action.admin
        })
    default:
      return state
  }
}
