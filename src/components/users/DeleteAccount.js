import React, { Component } from 'react'
import { connect } from 'react-redux'
import { startDeleteAccount } from '../../actions/users/accountActions'
import ConfirmationModal from '../../components/generics/ModalOneOption'


class DeleteAccount extends Component {
  state = {
    isDeletingAccount: false
  }

  handleDeleteAccountRequest = () => {
    this.props.deleteAccount()
  }

  handleOpenModal = () => {
    this.setState(() => ({
      isDeletingAccount: true
      })
    )
  }

  handleCloseModal = () => {
    this.setState(() => ({
      isDeletingAccount: false
      })
    )
  }

  render() {
    const {
      handleOpenModal,
      handleCloseModal,
      handleDeleteAccountRequest
    } = this

    return (
      <div className='mt-5'>
        <div className="card m-5 p-3 shadow-sm">
          <div className="card-body">
            <h3 className="font-weight-bold mb-3 border-bottom d-inline pb-1">🚨 Supprimer mon compte</h3>
            <div className='pl-5 mt-5'>
              <p className="card-text text-danger font-weight-bold mt-3">Cette action est irréversible.</p>
              <p>Nous ne divulguerons aucune information après la suppression. </p>
            <p className='font-weight-bold'>Après réussite de l'opération toutes vos informations seront supprimées.</p>
            </div>
            <button
              onClick={handleOpenModal}
              id='btn__delete-account'
              className='btn btn-danger btn-lg font-weight-bold'
            >
              Supprimer mon compte
            </button>

          </div>
        </div>
        <ConfirmationModal
          handleCloseModal={handleCloseModal}
          onRequestClose={handleCloseModal}
          isOpen={this.state.isDeletingAccount}
          contentLabel="Voulez-vous vraiment supprimer votre compte ?"
          handleClickAction={handleDeleteAccountRequest}
        />
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  deleteAccount: () => dispatch(startDeleteAccount())
})

export default connect(null, mapDispatchToProps)(DeleteAccount)
