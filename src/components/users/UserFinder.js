import React, { Component } from 'react'
import { connect } from 'react-redux'
import UserList from './UserList'
import { startSearchingUser  } from '../../actions/usersListActions'
import {
  setPseudoNameUsersListFilter,
  setDisplayAvatar,
  setDisplayUsersList
} from '../../actions/filterActions'

class UserFinder extends Component {
  componentDidMount() {
    this.props.setDisplayAvatarFilter(true)
  }

  toggleVisibility = (e) => {
    e.stopPropagation()
    const { isVisible, setVisibility } = this.props

    setVisibility(!isVisible)
  }

  handleSearch = (e) => {
    e.preventDefault()

    const pseudoName = e.target.elements.pseudoName.value.trim()

    this.props.setFilterPseudoUsersList(pseudoName)
    this.props.startSearchingUser()
  }

  render() {
    const { isVisible, usersResultInfos } = this.props
  return (
    <div className='container'>
        <p>Recherche un utilisateur : </p>
        <div>
          <form
            onSubmit={this.handleSearch}
          >
            <input
              name='pseudoName'
              type='text'
              placeholder="PseudoName"
            />
            <button>Rechercher</button>
          </form>
          <UserList
            className='user-finder__user-list'
            users={usersResultInfos}
          />
        </div>
    </div>
  )
  }
}

const mapStateToProps = (state) => {
  return  ({
    isVisible: state.filters.displayUsersList,
    usersResultInfos: state.usersList.resultItems
  })
}

const mapDispatchToProps = (dispatch) => ({
  startSearchingUser: () => dispatch(startSearchingUser()),
  setVisibility: (bool) => dispatch(setDisplayUsersList(bool)),
  setFilterPseudoUsersList: (pseudoName) => dispatch(setPseudoNameUsersListFilter(pseudoName)),
  setDisplayAvatarFilter: (display) => dispatch(setDisplayAvatar(display))

})

export default connect(mapStateToProps, mapDispatchToProps)(UserFinder)
