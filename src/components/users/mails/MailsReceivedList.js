import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  startFetchMessageReceived,
  startDeleteMessageReceived,
  startFetchMessageUnreadCounter,
  startDeleteAllMessageReceived
 } from '../../../actions/users/mails/mailActions'
import MailsListItem from './MailsListItem'
import ConfirmationModal from '../../generics/ModalOneOption'

class MailsReceivedList extends Component {
  state = {
    isDeleteAllModalOpen: false
}

  async componentDidMount() {
    await this.props.fetchMessageReceived()
  }

  componentDidUpdate(prevProps) {
    if(prevProps.unreadMsgCounter !== this.props.unreadMsgCounter) {
      this.props.fetchMessageReceived()
    }
  }

  handleDelete = (idMessage) => {
    const {
      deleteMessageReceived,
      fetchMessageReceived,
      fetchMessageUnreadCouter
    } = this.props

    deleteMessageReceived(idMessage)
    fetchMessageReceived()
    fetchMessageUnreadCouter()
  }

  handleDeleteAll = async () => {
    await this.props.deleteAllMessageReceived()
    this.props.fetchMessageReceived()
    this.handleCloseDeleteAllMsgModal()
  }

  handleOnClickLink = () => {
    this.props.history.push('/dashboard/mailbox/received')
  }

  handleOpenDeleteAllMsgModal = () => this.setState(() => ({ isDeleteAllModalOpen: true }))

  handleCloseDeleteAllMsgModal = () => this.setState(() => ({ isDeleteAllModalOpen: false }))

  render() {
    const { msgReceivedList } = this.props

    return (
      <div
        id='container__settings'
        className='border-left border-right'
      >
        <main
          role="main"
          className="inner cover p-5"
        >
          <div className='text-right'>
            {
              msgReceivedList.length !== 0 &&
                <button
                  className='btn btn-danger btn-lg font-weight-bold mb-5'
                  onClick={this.handleOpenDeleteAllMsgModal}
                >
                  Tout Supprimer
                </button>
            }
          </div>
          <ul className="list-group">
            {
              msgReceivedList && msgReceivedList.map((mail) =>
                    <MailsListItem
                      renderFrom
                      isDeletingAll={this.props.isDeletingAll}
                      isFetching={this.props.isFetching}
                      isDeletingOne={this.props.isDeletingOne}
                      key={mail._id}
                      {...mail}
                      handleDelete={this.handleDelete}
                    />
                  )
            }
          </ul>
          {
            msgReceivedList.length === 0 ? <p className='display-4 font-weight-bold'>Aucun Message</p> : false
          }
          <ConfirmationModal
            handleCloseModal={this.handleCloseDeleteAllMsgModal}
            onRequestClose={this.handleCloseDeleteAllMsgModal}
            isOpen={this.state.isDeleteAllModalOpen}
            handleClickAction={this.handleDeleteAll}
          />
          </main>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    msgReceivedList: state.messagesSpace.msgReceivedListItems,
    unreadMsgCounter: state.messagesSpace.unreadMsgCounter,
    isFetching: state.messagesSpace.msgReceivedFetching,
    isDeletingAll: state.messagesSpace.deletingAllMessageReceivedPending,
    isDeletingOne: state.messagesSpace.receivedMsgDeletePending
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchMessageReceived: () => dispatch(startFetchMessageReceived()),
  deleteMessageReceived: (idMessage) => dispatch(startDeleteMessageReceived(idMessage)),
  deleteAllMessageReceived: () => dispatch(startDeleteAllMessageReceived()),
  fetchMessageUnreadCouter: () => dispatch(startFetchMessageUnreadCounter())
})

export default connect(mapStateToProps, mapDispatchToProps)(MailsReceivedList)
