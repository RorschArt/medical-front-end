import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  startFetchMessageSended,
  startDeleteAllMessageSended,
  startDeleteMessageSended
} from '../../../actions/users/mails/mailActions'
import MailsListItem from './MailsListItem'
import ConfirmationModal from '../../generics/ModalOneOption'

class MailsSendedList extends Component {
  state = {
    isDeleteAllModalOpen: false
}

   componentDidMount() {
     this.props.fetchMessageSended()
  }

  handleDelete = (idMessage) => {
    this.props.deleteMessageSended(idMessage)
    this.props.fetchMessageSended()
  }

  handleOpenDeleteAllMsgModal = () => this.setState(() => ({ isDeleteAllModalOpen: true }))

  handleCloseDeleteAllMsgModal = () => this.setState(() => ({ isDeleteAllModalOpen: false }))

  handleDeleteAll = () => {
    this.props.deleteAllMessageSended()
    this.props.fetchMessageSended()
    this.handleCloseDeleteAllMsgModal()
  }

  render() {
    const { msgSendedList, isFetching, isDeletingAll, isDeletingOne } = this.props

    return (
      <div
        id='container__settings'
        className='border-left border-right'
      >
        <main
          role="main"
          className="inner cover p-5"
        >
          <div className='text-right'>
            {
              msgSendedList.length !== 0 &&
                <button
                  className='btn btn-danger btn-lg font-weight-bold mb-5'
                  onClick={this.handleOpenDeleteAllMsgModal}
                >
                  Tout Supprimer
                </button>
            }
          </div>
          <ul className="list-group">
          {
            msgSendedList && msgSendedList.map((mail) =>
                <MailsListItem
                  key={mail._id}
                  {...mail}
                  handleDelete={this.handleDelete}
                  isFetching={this.props.isFetching}
                  isDeletingOne={this.props.isDeletingOne}
                  isDeletingAll={this.props.isDeletingAll}
                />)
          }
          </ul>
          {
            msgSendedList.length === 0 ? <p className='display-4 font-weight-bold'>Aucun Message</p> : false
          }

          <ConfirmationModal
            emoticon='🗑️'
            title='Voulez-vous Vraiment Supprimer Tous les messages Envoyés ?'
            handleCloseModal={this.handleCloseDeleteAllMsgModal}
            onRequestClose={this.handleCloseDeleteAllMsgModal}
            isOpen={this.state.isDeleteAllModalOpen}
            contentLabel="Voulez-vous vraiment supprimer Tous les messages Envoyé ?"
            bodyText="Une Fois supprimé il vous sera impossible de les récupérer"
            classContainer='modal__title'
            modalStyle='modal-open'
            handleClickAction={this.handleDeleteAll}
            buttonNameRemoveModal="Non"
            buttonNameAction='Oui'
          />
          </main>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    msgSendedList: state.messagesSpace.msgSendedListItems,
    isFetching: state.messagesSpace.msgSendedFetching,
    isDeletingAll: state.messagesSpace.deletingAllMessageSendedPending,
    isDeletingOne: state.messagesSpace.savedMsgDeletePending
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchMessageSended: () => dispatch(startFetchMessageSended()),
  deleteAllMessageSended: () => dispatch(startDeleteAllMessageSended()),
  deleteMessageSended: (idMessage) => dispatch(startDeleteMessageSended(idMessage))
})

export default connect(mapStateToProps, mapDispatchToProps)(MailsSendedList)
