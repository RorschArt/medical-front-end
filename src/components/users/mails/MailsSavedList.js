import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  startFetchMessageSaved,
  startDeleteMessageSaved,
  startEditMessageSaved,
  startDeleteAllMessageSaved
} from '../../../actions/users/mails/mailActions'
import { setMsgSavedFocused } from '../../../actions/filterActions'
import MailsListItem from './MailsListItem'
import EditMessageModal from '../../generics/ModalEditMessage'
import ConfirmationModal from '../../generics/ModalOneOption'

class MailsSavedList extends Component {
  // Utilisé pour pouvoir manipuler la donnée envoyé dans l'action + tard
    state = {
      _id: null,
      header: null,
      textBody: null,
      from: null,
      isEditingMessage: false,
      isDeleteAllModalOpen: false
  }

  async componentDidMount() {
    await this.props.fetchMessageSaved()
  }
  // Voir Dans MailListItems Pour comprendre (fonctionIndirect)
  handleOpenModalEdit = ({ _id, header, textBody, from }) => {

    const message = { _id, header, textBody, from }

    this.setState(() => ({
      _id,
      header,
      textBody,
      from,
      isEditingMessage: true
    }))

    this.props.setMessageFocusedInState(message)
  }

  handleCloseModalEdit = () => this.setState(() => ({ isEditingMessage: false }))

  handleChangeHeader = (inputValue) => this.setState(() => ({ header: inputValue }))

  handleChangeTextBody = (inputValue) => this.setState(() => ({ textBody: inputValue }))

  handleDelete = (idMessage) => {
    const { deleteMessageSaved, fetchMessageSaved } = this.props

    deleteMessageSaved(idMessage)
    fetchMessageSaved()
  }

handleOpenDeleteAllMsgModal = () => this.setState(() => ({ isDeleteAllModalOpen: true }))

handleCloseDeleteAllMsgModal = () => this.setState(() => ({ isDeleteAllModalOpen: false }))

  handleDeleteAllSavedMsg = () => {
    this.handleCloseDeleteAllMsgModal()
    this.props.deleteAllMessagesSaved()
    this.props.fetchMessageSaved()
  }

  handleSave = () => {
    const { setMessageFocusedInState, editMessageSaved, fetchMessageSaved } = this.props
    // Utilisé pour enlever la propriété isEditingMessage et simplifier l'action
    const { _id, header, textBody, from } = this.state
    const message = { _id, header, textBody, from }

    setMessageFocusedInState(message)
    editMessageSaved()
    fetchMessageSaved()

    this.handleCloseModalEdit()
  }


  render() {
    const {
      handleOpenModalEdit,
      handleCloseModalEdit,
      handleSend,
      handleSave,
      handleChangeHeader,
      handleChangeTextBody,
      handleDelete,
      handleDeleteAllSavedMsg,
      handleCloseDeleteAllMsgModal,
      handleOpenDeleteAllMsgModal
    } = this

    return (
      <div
        id='container__settings'
        className='border-left border-right'
      >
      <main
        role="main"
        className="inner cover p-5"
      >
        <div className='text-right'>
          {
            this.props.msgSavedList.length !== 0 &&
              <button
                className='btn btn-danger btn-lg font-weight-bold mb-5'
                onClick={this.handleOpenDeleteAllMsgModal}
              >
                Tout Supprimer
              </button>
          }
        </div>
        <ul className="list-group">
          {
            this.props.msgSavedList && this.props.msgSavedList.map((mail) =>
                  <MailsListItem
                    renderEditButton
                    isDeletingAll={this.props.isDeletingAll}
                    isFetching={this.props.isFetching}
                    isDeletingOne={this.props.isDeletingOne}
                    handleEdit={handleOpenModalEdit}
                    key={mail._id}
                    {...mail}
                    handleDelete={this.handleDelete}
                  />
                )
          }
        </ul>
        {
          this.props.msgSavedList.length === 0 ? <p className='display-4 font-weight-bold'>Aucun Message</p> : false
        }
        </main>

        <EditMessageModal
          handleSave={handleSave}
          onRequestClose={handleCloseModalEdit}
          isOpen={this.state.isEditingMessage}
          contentLabel="Veuillez Editer votre messages"
          handleSubmit={handleSend}
          handleChangeHeader={handleChangeHeader}
          handleChangeTextBody={handleChangeTextBody}
        />
        <ConfirmationModal
          handleCloseModal={handleCloseDeleteAllMsgModal}
          onRequestClose={handleCloseDeleteAllMsgModal}
          isOpen={this.state.isDeleteAllModalOpen}
          contentLabel="Voulez-vous vraiment supprimer Tous les messages Sauvegardé ?"
          handleClickAction={handleDeleteAllSavedMsg}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    msgSavedList: state.messagesSpace.msgSavedListItems,
    isFetching: state.messagesSpace.msgSavedFetching,
    isDeletingAll: state.messagesSpace.deletingAllMessageSavedPending,
    isDeletingOne: state.messagesSpace.savedMsgDeletePending
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchMessageSaved: () => dispatch(startFetchMessageSaved()),
  deleteMessageSaved: (idMessage) => dispatch(startDeleteMessageSaved(idMessage)),
  deleteAllMessagesSaved: () => dispatch(startDeleteAllMessageSaved()),
  setMessageFocusedInState: (message) => dispatch(setMsgSavedFocused(message)),
  editMessageSaved: () => dispatch(startEditMessageSaved())
})

export default connect(mapStateToProps, mapDispatchToProps)(MailsSavedList)
