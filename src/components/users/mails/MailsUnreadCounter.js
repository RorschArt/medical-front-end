import React, { Component } from 'react'
import { startFetchMessageUnreadCounter } from '../../../actions/users/mails/mailActions'
import { connect } from 'react-redux'

class MailsUnreadCounter extends Component {
  state = {
    intervalId: undefined
  }

  componentDidMount() {
    this.props.fetchMessageUnreadCouter()

    let intervalId = setInterval(this.callbackInterval, 15000)
    this.setState(() => ({ intervalId: intervalId }))
  }

  callbackInterval = ()  => {
    this.props.fetchMessageUnreadCouter()
  }

  handleClick = ()  => {
    this.props.history.push('/dashboard/mailbox/received')
  }

  render() {
    const { unreadMsgCounter } = this.props

    return (
      <div
      className='counter-unread'
      onClick={this.handleClick}
      >
        <h2 className='counter-unread__title'>Messages Non lus</h2>
        <p className='counter-unread__number'>{unreadMsgCounter}</p>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    unreadMsgCounter: state.messagesSpace.unreadMsgCounter
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchMessageUnreadCouter: () => dispatch(startFetchMessageUnreadCounter())
})

export default connect(mapStateToProps, mapDispatchToProps)(MailsUnreadCounter)
