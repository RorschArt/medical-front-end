import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setSendingMailCompVisibility } from '../../../actions/filterActions'
import {
  startSendingMessage,
  startSavingMessage,
  startFetchMessageSaved,
  startFetchMessageSended,
  setContentMsgToSend
} from '../../../actions/users/mails/mailActions'
import SendMailForm from '../../users/mails/SendMailForm'
import UserFinder from '../../users/UserFinder'
import UserFocusedPreview from '../../users/UserFocusedPreview'

class SendMailPage extends Component {
  state = {
    error: undefined
  }

  toggleVisibility = (e) => {
    const { isHidden, setVisibility } = this.props

    setVisibility(!isHidden)
  }

  handleSaveMessage = async (header, textBody) => {
    const { saveMessage, fetchMessagesSaved } = this.props

    await saveMessage(header, textBody)
    fetchMessagesSaved()
    this.toggleVisibility()
  }

  handleSendingMessageClick = async (header, textBody) => {
    const {
      sendMessage,
      toggleVisibility,
      fetchMessagesSent,
      idUserFocused,
      userInfosFocused,
      setContentMsgToSend,
    } = this.props

    const contentMsg = { header, textBody }

    setContentMsgToSend(contentMsg)

    if (!idUserFocused && !userInfosFocused) {
      return this.setState(() => ({ error: `Vous n'avez pas saisi de destinataire` }))
    }

    await sendMessage(header, textBody, idUserFocused)

    this.toggleVisibility()
    fetchMessagesSent()
  }

  render () {
    const { isVisible, contentMsgToSend, isSending, isSaving } = this.props

    return (
      <div
        className='send-mail container-loader'
      >
        {
          isSending &&
          <div className="text-center">
            <div className="spinner-border" role="status">
            </div>
          </div> || isSaving &&
            <div className="text-center">
              <div className="spinner-border" role="status">
              </div>
            </div>
       }
          <UserFinder />
          <UserFocusedPreview />
          <div className='mail-form'>
            <SendMailForm
              contentMsgToSend={contentMsgToSend}
              error={this.state.error}
              handleSubmitUpload={this.handleSendingMessageClick}
              handleSave={this.handleSaveMessage}
            />
          </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  isVisible: state.filters.sendEmailCompVisible,
  idUserFocused: state.filters.userIdFocused,
  userInfosFocused: state.filters.userInfosFocused,
  contentMsgToSend: state.messagesSpace.contentMsgToSend,
  isSending: state.messagesSpace.sendingPendingMsg,
  isSaving: state.messagesSpace.savingPendingMsg
})

const mapDispatchToProps = (dispatch) => ({
  setVisibility: (bool) => dispatch(setSendingMailCompVisibility(bool)),
  sendMessage: (header, textBody, userId) => dispatch(startSendingMessage(header, textBody, userId)),
  saveMessage: (header, textBody, userId) => dispatch(startSavingMessage(header, textBody, userId)),
  fetchMessagesSaved: () => dispatch(startFetchMessageSaved()),
  fetchMessagesSent: () => dispatch(startFetchMessageSended()),
  setContentMsgToSend: (content) => dispatch(setContentMsgToSend(content))
})

export default connect(mapStateToProps, mapDispatchToProps)(SendMailPage)
