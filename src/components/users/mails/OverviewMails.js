import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import {
  startFetchMessageSendedCounter,
  startFetchMessageUnreadCounter,
  startFetchMessageSavedCounter,
  startFetchMessageCounter
} from '../../../actions/users/mails/mailActions'

// import MailsReceivedList from '../components/users/mails/MailsReceivedList'
// import MailsSavedList from '../components/users/mails/MailsSavedList'
// import MailsSendedList from '../components/users/mails/MailsSendedList'

class OverviewMails extends Component {
  state = {
    isHover1: undefined,
    isHover2: undefined,
    isHover3: undefined,
  }

  handleHover1 = (e) => {
    e.stopPropagation()

    this.setState(() => ({
      isHover1: true
    }))
  }

  handleMouseLeave1 = (e) => {
    e.stopPropagation()

    this.setState(() => ({
      isHover1: false
    }))
  }
  handleHover2 = (e) => {
    e.stopPropagation()

    this.setState(() => ({
      isHover2: true
    }))
  }

  handleMouseLeave2 = (e) => {
    e.stopPropagation()

    this.setState(() => ({
      isHover2: false
    }))
  }
  handleHover3 = (e) => {
    e.stopPropagation()

    this.setState(() => ({
      isHover3: true
    }))
  }

  handleMouseLeave3 = (e) => {
    e.stopPropagation()

    this.setState(() => ({
      isHover3: false
    }))
  }
  componentDidMount() {
    this.props.fetchMessageUnreadCounter()
    this.props.fetchMessageSendedCounter()
    this.props.fetchMessageSavedCounter()
    this.props.fetchMessagReceived()
  }

  render() {
    return (
      <div className='shadow mr-5'>



        <ul className="border-0 list-group text-capitalize ">
          <NavLink
            exact
            to='/dashboard/mailbox/received'
            className='text-decoration-none'
          >
            <li
              onMouseOver={this.handleHover1}
              onMouseLeave={this.handleMouseLeave1}
              className={this.state.isHover1 ?
                `list-group-item d-flex justify-content-between align-items-center list-group-item-primary`
                  : `list-group-item d-flex justify-content-between align-items-center`
              }
            >
              <p className='m-2'>Messages reçus</p>
              {
                this.props.isFetching &&
                  <div >
                    <div className="text-center">
                      <div className="spinner-border  text-primary" role="status">
                      </div>
                    </div>
                  </div>
              }
              {
                !this.props.isFetching &&
                  <span className="badge badge-primary badge-pill">{this.props.receivedMsgCounter}</span>
              }

              {
                this.props.unreadMsgCounter > 0 && <span className="badge badge-primary badge-pill badge-danger">{this.props.unreadMsgCounter}</span>
              }
            </li>
          </NavLink>
          <NavLink
            exact
            to='/dashboard/mailbox/sended'
            className='text-decoration-none'
          >
            <li
              onMouseOver={this.handleHover2}
              onMouseLeave={this.handleMouseLeave2}
              className={this.state.isHover2 ?
                  "list-group-item d-flex justify-content-between align-items-center list-group-item-primary"
                    : "list-group-item d-flex justify-content-between align-items-center"}
            >
              <p className='m-2'>Messages envoyés</p>
              { !this.props.isFetching &&
                 <span className="badge badge-primary badge-pill">{this.props.sendedMsgCounter}</span>
              }
              {
                this.props.isFetching &&
                <div >
                  <div className="text-center">
                    <div className="spinner-border  text-primary" role="status">
                    </div>
                  </div>
                </div>
              }
            </li>
          </NavLink>
          <NavLink
            exact
            to='/dashboard/mailbox/saved'
            className='text-decoration-none'
          >
          <li
            onMouseOver={this.handleHover3}
            onMouseLeave={this.handleMouseLeave3}
            className={this.state.isHover3 ?
              "list-group-item d-flex justify-content-between align-items-center list-group-item-primary"
                : "list-group-item d-flex justify-content-between align-items-center"}
          >
            <p className='m-2 mr-5'>Messages sauvegardés</p>
            { !this.props.isFetching &&
                <span className="badge badge-primary badge-pill">{this.props.savedMsgCounter}</span>
            }
            {
              this.props.isFetching &&
              <div >
                <div className="text-center">
                  <div className="spinner-border  text-primary" role="status">
                  </div>
                </div>
              </div>
            }
          </li>
          </NavLink>
            <NavLink
              exact
              to='/dashboard/mailbox/create'
              className='text-decoration-none '
            >
            <li
              className="list-group-item bg-warning text-dark text-uppercase font-weight-bold align-items-center text-center  border-0 rounded-0"
            >
              <div >
                <p className='m-2 pr-4 '>Ecrire</p>
              </div>
            </li>
            </NavLink>
        </ul>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    receivedMsgCounter: state.messagesSpace.receivedCounter,
    unreadMsgCounter: state.messagesSpace.unreadMsgCounter,
    sendedMsgCounter: state.messagesSpace.sendedCounter,
    savedMsgCounter: state.messagesSpace.savedCounter,
    isFetching: state.messagesSpace.msgReceivedFetching,
    isDeletingAll: state.messagesSpace.deletingAllMessageReceivedPending,
    isDeletingOne: state.messagesSpace.receivedMsgDeletePending
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchMessagReceived: () => dispatch(startFetchMessageCounter()),
  fetchMessageUnreadCounter: () => dispatch(startFetchMessageUnreadCounter()),
  fetchMessageSavedCounter: () => dispatch((startFetchMessageSendedCounter())),
  fetchMessageSendedCounter: () => dispatch(startFetchMessageSavedCounter())
})

export default connect(mapStateToProps, mapDispatchToProps)(OverviewMails)
