import React, { Component } from 'react'
import Avatar from '../Avatar'
import moment from '../../../helpers/moment'

export default class MailsListItem extends Component {
  state = {
    toggled: false,
    expandTransition: false
  }

  handleCollapseToggle = () => {
    this.setState((prevState) => ({
      expandTransition: true
    }))

    this.setState((prevState) => ({
      toggled: !prevState.toggled
    }))
    this.setState((prevState) => ({
      expandTransition: false
    }))
  }

  handleDeleteClick = () => {
    this.props.handleDelete(this.props._id)
  }

  handleEditClick = () => {
    this.props.handleEdit(this.props)
  }

  render() {
    const date = moment.utc(this.props.createdAt, 'YYYY MM DD  HH:mm:ss ZZ')
    const dateParsed = date.format('dddd MMMM DD')

    const {
      header,
      textBody,
      from,
      renderFrom,
      renderEditButton,
      handleEdit,
      isDeletingAll,
      isDeletingOne,
      isFetching,
      unRead,
      _id
    } = this.props

    return (
      <div className='d-flex p-3'>
      {
        unRead
      }
        <li className={!unRead ? 'list-group-item p-5 flex-fill' : 'list-group-item p-5 flex-fill list-group-item-danger'}>
        <div className='d-flex flex-row border-bottom pb-3'>
            {
              renderFrom &&
                <h4 className='mb-0 mt-3 text-uppercase w-50'>Ecrit par : </h4>
            }
            {
                renderFrom &&
                  <div className='ml-5'>
                    <Avatar
                      avatarId={from.user.avatarId}
                      user={from}
                      showAvatarImage
                      size={{width: 3 + 'rem'}}
                    />
                  </div>
            }
            <div className='text-right w-100'>
            </div>
            <a className='font-weight-light w-50'>{dateParsed}</a >
            </div>
          <div className='d-flex justify-content-between mt-5'>
        <a className='font-weight-bold'>{header}</a >
        {
          isDeletingAll  &&
          <div className="text-center">
          <div className="spinner-border" role="status">
          </div>
          </div>
          || isFetching &&
          <div className="text-center">
            <div className="spinner-border" role="status">
            </div>
          </div>
          || isDeletingOne &&
          <div className="text-center">
          <div className="spinner-border" role="status">
          </div>
          </div>
        }
          <button
            className="btn btn-primary m-3"
            type="button"
            data-toggle="collapse"
            data-target={_id}
            aria-expanded="false"
            aria-controls="mailsReceived"
            onClick={this.handleCollapseToggle}
          >
          Options ⚙️
          </button>
          </div>
        <div
          className={this.state.toggled ? 'collapse show' : 'collapse collapse-transition '}
          id="mailsReceived"
        >
          <div className="card card-body bg-light text-dark p-5">
            {textBody}
          </div>

        {
          this.state.toggled &&
            <button
              className='btn btn-danger m-3'
              onClick={this.handleDeleteClick}
            >
              Supprimer
            </button>
        }
        {
          this.props.renderEditButton &&
            <button
              className='btn btn-warning m-3'
              onClick={this.handleEditClick}
            >
              Editer
            </button>
        }
          </div>
            </li>
      </div>
    )

  }
}
