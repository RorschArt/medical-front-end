import React, { Component } from 'react'
import { connect } from 'react-redux'
import { startSendingMessage, startSavingMessage } from '../../../actions/users/mails/mailActions'

class SendMailForm extends Component {
  state = {
    error: undefined,
    header: null,
    textBody: null
  }

  handleChangeHeader = (e) => {
    e.preventDefault()

    let header = e.target.value
    this.setState(() => ({
      header: header
    }))
  }

  handleChangeTextBody = (e) => {
    e.preventDefault()

    let textBody = e.target.value
    this.setState(() => ({
      textBody: textBody
    }))
  }

  handleSave = () => {
    const header = this.state.header
    const textBody = this.state.textBody

    this.props.saveMessage(header, textBody)
    this.props.history.push('/dashboard')
  }

  handleSubmit = async (e) => {
    e.preventDefault()

    const header = e.target.elements.header.value.trim()
    const textBody = e.target.elements.textBody.value.trim()

    if(header.length < 3)
    return this.setState(() => ({
      error: 'Veuillez enter un objet de 3 caractères minimum'
    }))

    if(textBody.length < 10)
    return this.setState(() => ({
      error: 'Veuillez écrire au moins 20 caractères dans votre message'
    }))

    const adminId = this.props.adminSelected._id

    await this.props.sendMessage(header, textBody, adminId)
    this.props.history.push('/dashboard')
  }

  render () {

    return (
      <form onSubmit={this.handleSubmit}>
        <div className="input-group mb-5 d-flex p-5">
        { this.state.error && <p className='error-msg'>{this.state.error}</p>}
          <div className="input-group-prepend">
            <input className='input-group-text'/>
          </div>
            <input onChange={this.handleChangeHeader} type="text" name='header'className="form-control" placeholder="Objet" aria-label="Objet" aria-describedby="Objet"/>
          </div>
          <div className='p-5'>
            <textarea onChange={this.handleChangeTextBody} type="text" name='textBody' id='textarea-msg' className="form-control bg-light" placeholder="Votre Message" aria-label="Objet" aria-describedby="Objet"/>
          </div>
          <div className='d-flex justify-content-between'>
            <button
              className='btn btn-danger m-5'
              type='submit'
            >
              Envoyer
            </button>
            <button
              onClick={this.handleSave}
              type='submit'
              className='btn btn-warning m-5'
            >
              Sauvegarder
            </button>
        </div>
      </form>
    )
  }
}

const mapStateToProps = (state) => {
return  ({
    adminSelected: state.dashboard.adminSelectedMsg
  })
}


const mapDispatchToProps = (dispatch) => {
  return {
    sendMessage: async (header, textBody, adminId) => dispatch(startSendingMessage(header, textBody, adminId)),
    saveMessage: async (header, textBody) => dispatch(startSavingMessage(header, textBody))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SendMailForm)
