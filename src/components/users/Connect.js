import React, { Component } from 'react'
import { connect } from 'react-redux'
import { startLogin } from '../../actions/loginActions'
import ModalConnect from './ModalConnect'

class Connect extends Component {
  state = {
      isOpen: false
  }

  toggleModal = () => {
    this.setState((prevState) => ({
      isOpen : !prevState.isOpen
    }))
  }
  // Une arrow fonction pour avoir accés a this.
  handleSubmit = async (email, password) => {
    try {
      this.props.startLogin(email.trim(), password.trim())
    } catch (err) {
      throw new Error()
    }
  }

  render () {
    return (
      <div>
      <button
        id='btn-connect'
        className='btn btn-primary'
        onClick={this.toggleModal}>
          Connexion
      </button>
      <ModalConnect
        handleCloseModal={this.toggleModal}
        onRequestClose={this.toggleModal}
        isOpen={this.state.isOpen}
        contentLabel="connectez-vous"
        handleConnexion={this.handleSubmit}
      />
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    startLogin: async (email, password) => dispatch(startLogin(email, password))
  }
}

export default connect(null, mapDispatchToProps)(Connect)
