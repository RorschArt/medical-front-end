import React, { Component } from 'react'
import { connect } from 'react-redux'
import { startAccountCreation } from '../../actions/accountCreationActions'
import ModalCreateAccount from './ModalCreateAccount'

class AddArticlePage extends Component {
  state = {
      isOpen: false
  }

  toggleModal = () => {
    this.setState((prevState) => ({
      isOpen : !prevState.isOpen
    }))
  }

  handleCreationAction = (email, password, pseudoName) => {
    try {
      const { startCreationUser } = this.props

      startCreationUser(email.trim(), password.trim(), pseudoName.trim())
    } catch (err) {
      throw new Error()
    }
  }

  render () {
    return (
      <div>
        <button
          id='btn-create-account'
          className='btn btn-danger'
          onClick={this.toggleModal}>
            Créer mon compte
        </button>
        <ModalCreateAccount
          handleCloseModal={this.toggleModal}
          onRequestClose={this.toggleModal}
          isOpen={this.state.isOpen}
          contentLabel="connectez-vous"
          handleCreationAction={this.handleCreationAction}
        />
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  startCreationUser: (email, passord, pseudoName) => dispatch(startAccountCreation(email, passord, pseudoName))
})

export default connect(null, mapDispatchToProps)(AddArticlePage)
