import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  deleteUserInfosFocused,
  deleteUserIdFocused
} from '../../actions/filterActions'
import Avatar from '../users/Avatar'

class UserFocusedPreview extends Component {
  handleClick = () => {
    this.props.deleteUserFocused()
    this.props.deleteUserIdFocused()
  }

  render() {
    const { user, displayUserPreview } = this.props
    
  return (
    <div
      className={ displayUserPreview ?
          'user-preview' : 'user-preview--hidden'}
      onClick={this.handleClick}
    >
      <p className='user-preview__title'>Destinataire : </p>
      {
        user && <Avatar
                  userFocused={user}
                  avatarId={user.avatarId}
                  showAvatarImage
                />
      }
    </div>

  )
  }
}

const mapStateToProps = (state) => {
  return  ({
    user: state.filters.userFocused,
    displayUserPreview: state.filters.displayUserPreview
  })
}

const mapDispatchToProps = (dispatch) => ({
  deleteUserFocused: () => dispatch(deleteUserInfosFocused()),
  deleteUserIdFocused: () => dispatch(deleteUserIdFocused())
})

export default connect(mapStateToProps, mapDispatchToProps)(UserFocusedPreview)
