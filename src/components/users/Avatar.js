import React, { Component } from 'react'
import FileUploadDropZone from '../generics/FileUploadDropZone'
import 'react-toastify/dist/ReactToastify.css'
import { connect } from 'react-redux'
import { startUploadAvatar } from '../../actions/users/dashboardAction'
import uuid from 'uuid'
const url = process.env.MONGODB_DATABASE_URL

class Avatar extends Component {
    handleAvatarUpload = (e) => {
      const avatarFile = e.target.files[0]
      this.props.uploadAvatarAction(avatarFile)
    }

  render() {

    const {
      user,
      needButtonUpload,
      showAvatarImage,
      isUploading,
      avatarId,
      classImg,
      size,
      fontSize
    } = this.props

  return (
    <div className="">
        <div className="card shadow text-center position-relative" style={size}>
          {isUploading &&
            <img src='images/avatar1.png' className="card-img-top"/>
          }
          {!avatarId &&
            <img src='images/avatar1.png' className="card-img-top"/>
          }
          {!isUploading ? avatarId &&
            <>
              <img src={`${url}/users/${avatarId}/avatar/${uuid()}`} className="card-img-top"/>
              <img src='images/avatar1.png' className="d-none card-img-top"/>
            </>
              :
            <>
              <img src={`${url}/users/${avatarId}/avatar/${uuid()}`} className="d-none card-img-top"/>
              <img src='images/avatar1.png' className="d-none card-img-top"/>
            </>
            ? !avatarId &&
            <>
              <img src={`${url}/users/${avatarId}/avatar/${uuid()}`} className="d-none card-img-top"/>
              <img src='images/avatar1.png' className=" card-img-top"/>
            </>
            : false
          }
          <div className="card-body p-0">
            {user.data && <h5 className="card-title mb-1 text-capitalize">{user.data.user.pseudoName}</h5>}
            {!user.data && <h5 className={`card-title mb-1 text-capitalize`}><small>{user.user.pseudoName}</small></h5>}
            {
              needButtonUpload &&
                <FileUploadDropZone
                  handleUploadImage={this.handleAvatarUpload}
                  classStyleDropZone='button button--input-file'
                />
            }

          </div>
          {
            isUploading &&
            <div className="avatar">
            <div className="spinner-border text-danger" role="status">
            </div>
            </div>
          }
        </div>
    </div>
  )
  }
}


const mapStateToProps = (state) => {
return  ({
    isUploading: state.dashboard.uploadAvatarPending
  })
}

const mapDispatchToProps = (dispatch) => ({
  uploadAvatarAction: (avatarFile) => dispatch(startUploadAvatar(avatarFile))
})

export default connect(mapStateToProps, mapDispatchToProps)(Avatar)
