import React, { Component } from 'react'
import Modal from 'react-modal'
import isEmail from 'validator/lib/isEmail';


const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    width                 : '50rem',
    height                 : '40rem',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
}

export default class ModalConnect extends Component {
  state = {
    passwordError: undefined,
    emailError: undefined
  }

  componentDidMount = () => {
    Modal.setAppElement('body')
  }

  handleReturn = (e) => {
      e.preventDefault()
      this.props.handleCloseModal()
  }

  handleClick = (e) => {
    e.preventDefault()
    const email = e.target.elements.inputEmail.value.trim()
    const password = e.target.elements.inputPassword.value.trim()

    this.setState(() => ({
        passwordError: undefined,
        emailError: undefined
      }))

    if (password.length < 6) {
      return this.setState(() => ({
          passwordError: 'Le Mot de Passe doit contenir 6 caractères.'
        })
      )
    }

    if(!isEmail(email)) {
      return this.setState(() => ({
          emailError: 'Email non valide.'
        })
      )
    }

    this.props.handleConnexion(email, password)
    }

    render = () => {
      const {
        isOpen,
        contentLabel,
        handleClickAction,
        handleCloseModal,
        classContainer,
      } = this.props

      return (
        <Modal
          isOpen={isOpen}
          onRequestClose={handleCloseModal}
          ariaHideApp={false}
          contentLabel={contentLabel}
          closeTimeoutMS={200}
          style={customStyles}
        >
          <div className='modal-open'>
            <form
              onSubmit={this.handleClick}
              className="form-signin"
            >
              <h1
                className="modal__title"
              >
                👩‍⚕️ Connectez-vous.
              </h1>
              {this.state.emailError && <p className='error-msg'>{this.state.emailError}</p>}
              <label htmlFor="inputEmail" className="sr-only">Adresse Email</label>
              <input
                type="email"
                name="inputEmail"
                className="form-control"
                placeholder="✉️   Adresse Email"
                required
                autoFocus
              />
              {this.state.passwordError && <p className='error-msg'>{this.state.passwordError}</p>}
              <label htmlFor="inputPassword" className="sr-only">Mot de Passe</label>
              <input
                type="password"
                name="inputPassword"
                className="form-control"
                placeholder="🔒   Mot de passe"
                required
              />
              <div className="checkbox mb-3">
                <label className='modal__checkbox-text'
>
                  <input
                    className='modal__checkbox'
                    type="checkbox"
                    value="Se rappeler de moi"
                  />
                    Se souvenir de moi
                </label>
              </div>
              <button
                className="btn btn-primary modal__button-connect"
                type="submit"
              >
              Se connecter
              </button>
              <button
                onClick={this.handleReturn}
                className="btn btn-secondary modal__button-return"
                type="submit"
              >
                Retour
              </button>
            </form>
           </div>
        </Modal>
      )
    }
}
