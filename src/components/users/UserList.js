import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  setUserIdFocused,
  setUserPreviewFocusedVisibility,
  setUserInfosFocused
} from '../../actions/filterActions'
import UserListItem from './UserListItem'

class UserList extends Component {
  handleClickUser = (user) => {
      this.props.setIdUserFocused(user._id)
      this.props.setUserInfosFocused(user.user)
      this.props.displayUserPreview(true)
  }

  render() {
    const { user, displayAvatar, isFetching, className } = this.props
    return (
      <div className='container-loader users-list'>
      <p>Clique dessus pour l'ajouter au destinataire</p>
        <div>
          {
            user && <UserListItem
                      className={className}
                      handleClick={this.handleClickUser}
                      user={user}
                      displayAvatar
                    />
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.usersList.resultItem,
    displayAvatar: state.displayAvatar,
    isFetching: state.usersList.isFetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setIdUserFocused: (userId) => dispatch(setUserIdFocused(userId)),
    displayUserPreview: (bool) => dispatch(setUserPreviewFocusedVisibility(bool)),
    setUserInfosFocused: (user) => dispatch(setUserInfosFocused(user))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)
