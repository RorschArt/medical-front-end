import React, { Component } from 'react'
import Modal from 'react-modal'
import isEmail from 'validator/lib/isEmail'


const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    width                 : '50rem',
    height                 : '40rem',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
}

export default class ModalConnect extends Component {
  state = {
    passwordError: undefined,
    emailError: undefined,
    userNameError: undefined
  }

  componentDidMount = () => {
    Modal.setAppElement('body')
  }

  handleReturn = (e) => {
      e.preventDefault()
      this.props.handleCloseModal()
  }

  handleClick = (e) => {
    e.preventDefault()
    const email = e.target.elements.inputEmail.value.trim()
    const password = e.target.elements.inputPassword.value.trim()
    const pseudoName = e.target.elements.inputUserName.value.trim()

    if (password.length < 6) {
      return this.setState(() => ({
          passwordError: 'Le Mot de Passe doit contenir 6 caractères minimum.'
        })
      )
    }

    if (pseudoName.length < 4) {
      return this.setState(() => ({
          userNameError: 'Le pseudo doit contenir 4 caractères minimum.'
        })
      )
    }

    if(!isEmail(email)) {
      return this.setState(() => ({
          emailError: 'Email non valide.'
        })
      )
    }
    
    this.props.handleCreationAction(email, password, pseudoName)
  }

    render = () => {
      const {
        isOpen,
        contentLabel,
        handleClickAction,
        handleCloseModal,
        classContainer,
      } = this.props

      return (
        <Modal
          isOpen={isOpen}
          onRequestClose={handleCloseModal}
          ariaHideApp={false}
          contentLabel={contentLabel}
          closeTimeoutMS={200}
          style={customStyles}
        >
          <div className='modal-open'>
          <form
            onSubmit={this.handleClick}
            className="form-signin"
          >
            <h1
              className="modal__title"
            >
              ✍️  Création de votre compte
            </h1>
            {this.state.emailError && <p className='error-msg'>{this.state.emailError}</p>}
            <label htmlFor="inputEmail" className="sr-only">Adresse email</label>
            <input
              type="email"
              name="inputEmail"
              className="form-control"
              placeholder="✉️   Adresse Email"
              required
              autoFocus
            />
            {this.state.passwordError && <p className='error-msg'>{this.state.passwordError}</p>}
            <label htmlFor="inputPassword" className="sr-only">Mot de Passe</label>
            <input
              type="password"
              name="inputPassword"
              className="form-control"
              placeholder="🔒   Mot de passe"
              required
            />
            {this.state.userNameError && <p className='error-msg'>{this.state.userNameError}</p>}
            <label htmlFor="inputUserName" className="sr-only">Mot de Passe</label>
            <input
              type="text"
              name="inputUserName"
              className="form-control"
              placeholder="👤    Pseudo name"
              required
            />
            <button
              className="btn btn-danger modal__button-connect"
              type="submit"
            >
            Créer mon compte
            </button>
            <button
              onClick={this.handleReturn}
              className="btn btn-secondary modal__button-return"
              type="submit"
            >
              Retour
            </button>
          </form>
          </div>
        </Modal>
      )
    }
}
