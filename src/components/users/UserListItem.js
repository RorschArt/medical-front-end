import React, { Component } from 'react'
import ImageCard from '../generics/ImageCard'

 class UserListItem extends Component {
   handleOnClick = () => {
     this.props.handleClick(this.props.user)
   }

   render() {
     const { user, displayAvatar, classContainer, classImg, className } = this.props

    return (
        <div
         onClick={this.handleOnClick}
         className={className}
        >
          <p className='main-card__pseudoName'>{user.user.pseudoName}</p>
          {
           displayAvatar &&
             <ImageCard
               avatarCard
               avatarId={user.user.avatarId}
               classContainer={classContainer}
               classImg={classImg}
             />
          }
        </div>
    )
   }
}

export default UserListItem
