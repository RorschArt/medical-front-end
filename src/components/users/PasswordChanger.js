import React, { Component } from 'react'
import ModalPasswordChecker from '../generics/ModalPasswordChanger'
import { connect } from 'react-redux'
import { startResetPassword, showModalResetPassword } from '../../actions/users/accountActions'

class PasswordChanger extends Component {
  handleResetPasswordClick = async (e) => {
    const { handleResetPassword } = this.props

    const oldPassword = e.target.elements.oldPassword.value.trim()
    const newPassword = e.target.elements.newPassword.value.trim()
    const confirmPassword = e.target.elements.confirmPassword.value.trim()
    // TODO: async dans un component pas optimal
    await handleResetPassword(oldPassword, newPassword, confirmPassword)
  }

  handleOpenModal = () => {
    this.props.displayModal(true)
  }

  handleCloseModal = (e) => {
    if (e) {
      e.preventDefault()
    }
    this.props.displayModal(false)
  }

  render() {
    const {
      handleOpenModal,
      handleResetPasswordClick,
      handleCloseModal,
    } = this

    return (
      <div className='mt-5'>
        <div className="card m-5 p-3 shadow-sm">
          <div className="card-body">
            <h3 className="font-weight-bold mb-3 border-bottom d-inline pb-1">🔒 modifier mon mot de passe</h3>
            <div className='pl-5 mt-5'>
              <p className="card-text">Je souhaite Modifier mon mot de passe.</p>
            </div>
            <button
              onClick={handleOpenModal}
              id='btn__change-password'
              className='btn btn-primary btn-lg'
            >
              Modifier
            </button>

          </div>
        </div>
        <ModalPasswordChecker
          loaderDisplay={this.props.passwordResetPending}
          handleCloseModal={handleCloseModal}
          onRequestClose={handleCloseModal}
          isOpen={this.props.displayModalResetPassword}
          contentLabel="Voulez-vous vraiment supprimer votre compte ?"
          handleClickAction={handleResetPasswordClick}
          />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return ({
    passwordResetPending: state.dashboard.passwordCheckingPending || state.dashboard.resetPasswordPending,
    displayModalResetPassword: state.dashboard.displayModalResetPassword
  })
}

const mapDispatchToProps = (dispatch) => ({
  handleResetPassword: (oldPassword, newPassword, confirmPassword) => {
    dispatch(startResetPassword(oldPassword, newPassword, confirmPassword))
  },
  displayModal: (bool) => dispatch(showModalResetPassword(bool))
})

export default connect(mapStateToProps, mapDispatchToProps)(PasswordChanger)
