import React, { Component } from 'react'
const url = process.env.MONGODB_DATABASE_URL

class ArticleListItem extends Component {

  handleClickArticle = () => {
    this.props.handleClick(this.props.article)
  }

  render() {
    const { title, subTitle, textBodyArticle, _id, imageId } = this.props.article

    return (
      <div onClick={this.handleClickArticle} className="card mt-3 mb-5 shadow-sm" style={{ cursor: 'pointer' }}>
          <div  className="card" style={{width: 40 + 'rem'}}>
            <img src={`${url}/articles/${imageId}/image`} className="card-img-top" />
            <div className="card-body">
              <h3 className="font-weight-bold">{title}</h3>
              <h6 className="card-title">{subTitle}</h6>
            </div>
          </div>
      </div>
    )
}
}

export default ArticleListItem
