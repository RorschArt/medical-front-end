import React from 'react'
import { Link } from 'react-router-dom'

const NotFoundPage = (props) => {
  return (
      <div className="not-found">
        Je suis la page 404 - <Link to='/'>Page d'acceuil</Link>
      </div>
  );
};

NotFoundPage.defaultProps = {
}

export default NotFoundPage