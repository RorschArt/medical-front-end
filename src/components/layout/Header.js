import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import Connect from '../users/Connect'
import CreateAccount from '../users/CreateAccount'
import Avatar from '../users/Avatar'

class Header extends Component {

  render() {
    const {
      links,
      needButtonDisconnect,
      needUnreadCounter,
      handleDisconnect,
      userHeader,
      title,
      admin,
      user,
      dashboard,
      history
    } = this.props

    return (
      <div>
      <header className={userHeader ? "blog-header fixed-top p-2 shadow-sm header--user" : "blog-header fixed-top p-2 shadow-sm" }>
    <div className="row flex-nowrap justify-content-between align-items-center">
      <div className="row-4 pt-1">
      </div>
          {
            links.map((link) => (
              <NavLink
                key={link.url}
                className='nav-link header__link'
                exact
                to={`${link.url}`}
              >
                {link.name}
              </NavLink>
            ))
          }
      <div className="col-4 d-flex justify-content-end align-items-center">
          {
            !needButtonDisconnect &&
                <Connect />
          }
          {
            !needButtonDisconnect &&
                <CreateAccount />
          }
          {
            needButtonDisconnect &&
              <button
              id='btn-disconnect'
              onClick={handleDisconnect}
              className="btn btn-sm btn-danger"
              >
              Déconnexion
              </button>
          }
          { needButtonDisconnect &&
            <div className='mr-5'>
            <NavLink
              exact
              to='/dashboard/settings'
              className=''
            >
            <div className=''>
              <Avatar
              avatarId={dashboard.avatarId}
              user={{user:{}}}
              showAvatarImage
              size={{width: 2 + 'rem'}}
              />
              </div>
            </NavLink>
            </div>
          }
              </div>
            </div>
          </header>
      </div>
    )
  }

}


export default Header
