import React from 'react'

const Footer = ({ admin }) => {
  return (
      <div
        className={ !admin ? 'footer' : 'footer footer--admin'}
      >
      </div>
  )
}

Footer.defaultProps = {
}

export default Footer
