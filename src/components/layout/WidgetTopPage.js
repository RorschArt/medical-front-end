import React from 'react'

const WidgetTopPage = (props) => {
  return (
    <div className='widget-top-page'>
      <h3 className='widget-top-page__title'>Nouveau: Créez votre compte !</h3>
    </div>
  )
}

WidgetTopPage.defaultProps = {
}

export default WidgetTopPage
