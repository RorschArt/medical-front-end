import React from 'react'
import { connect } from 'react-redux'
import * as yup from 'yup'
import { withFormik, Form, Field, ErrorMessage } from 'formik'

const FormikForm = (props, {
  values,
  errors,
  touched,
  isSubmitting,
  email,
  password,
  pseudoName
}) => {
  if (props.loginForm) {
    return (
      <Form>
        <div>
          <Field
            type='email'
            name='email'
            value={email}
            placeholder='Email'
          />
          <ErrorMessage className='error-msg error-msg--white' name='email' />
          <Field
            type='password'
            name='password'
            value={password}
            placeholder='Mot de Passe'
          />
          <ErrorMessage className='error-msg error-msg--white' name='password' />
          <button
          className='btn btn-danger'
          disabled={isSubmitting}
          type='submit'
          >
          Se Connecter
          </button>
        </div>
      </Form>
    )
  } else if (props.createAccountForm) {
      return (
        <Form>
          <div>
            <ErrorMessage className='error-msg error-msg--white' name='email' />
            <Field
              type='email'
              name='email'
              value={email}
              placeholder='Email'
            />
            <ErrorMessage className='error-msg error-msg--white' name='password' />
            <Field
              type='password'
              name='password'
              value={password}
              placeholder='Mot de Passe'
            />
            <Field
              type='text'
              name='pseudoName'
              value={pseudoName}
              placeholder='Mon Pseudo'
            />
          </div>
          <button
            className='btn btn-danger'
            type='submit'
            disabled={isSubmitting}
          >
          Créer Un Compte
        </button>
        </Form>
      )
    }
}

const BasicForm = withFormik({
  mapPropsToValues (props) {
    return {
      email: '',
      password: '',
      pseudoName: '',
      title: '',
      subTitle:'',
      textBodyArticle: ''
    }
  },
  validationSchema: yup.object().shape({
    email: yup.string()
      .email('L\'email n\'est pas valide')
      .required('L\'Email est obligatoire'),
    password: yup.string()
      .min(6, 'Le mot de passe doit contenir au moins 5 caractère')
      .required('Le mot de passe est obligatoire')
  }),
  async handleSubmit (values,
    { props,
      resetForm,
      setErrors,
      setSubmitting
    }) {
    try {
      if(values.email) {
        await props.handleSubmit(values.email, values.password, values.pseudoName)
      }
      setSubmitting(false)
    } catch (err) {
      setSubmitting(false)
      // await resetForm() TODO: DONNE UNE ERREUR EN LOG
    }
  }
})(FormikForm)

const mapStateToProps = (state) => ({
  user: state.user
})
export default connect(mapStateToProps)(BasicForm)
