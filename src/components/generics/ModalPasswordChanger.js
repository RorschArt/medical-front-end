import React, { Component } from 'react'
import Modal from 'react-modal'

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    width                 : '50rem',
    height                 : '40rem',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
}

export default class ModalPasswordChecker extends Component {
  state = {
    error: undefined
  }
  handleClick = (e) => {
    e.preventDefault()
    const newPassword = e.target.elements.newPassword.value.trim()
    const oldPassword = e.target.elements.oldPassword.value.trim()
    // Back end demande un minimum de 6, valeur également dans le DOM (voir l15)
    if (newPassword.length < 6) {
      return this.setState(() => ({
          error: 'Le Mot de Passe doit contenir 6 caractères.'
        })
      )
    } else if (newPassword === oldPassword) {
      return this.setState(() => ({
          error: 'Les mots de passe sont identiques.'
        })
      )
    }

    this.props.handleClickAction(e)
  }

  componentDidMount = () => {
    Modal.setAppElement('body')
  }

    render = () => {
      const {
        isOpen,
        contentLabel,
        handleCloseModal,
        classContainer,
        loaderDisplay
      } = this.props

      return (
        <Modal
          isOpen={isOpen}
          onRequestClose={handleCloseModal}
          ariaHideApp={false}
          contentLabel={contentLabel}
          closeTimeoutMS={200}
          style={customStyles}
        >
          <div className='modal-open'>
            <form
              onSubmit={this.handleClick}
              className="form-signin"
            >
              <h1
                className="modal__title"
              >
                🖋️  Changer mon mot de passe
              </h1>
              {this.state.error && <p className='error-msg'>{this.state.error}</p>}
              <label htmlFor="inputEmail" className="sr-only">Ancien mot de passe</label>
              <input
                type="password"
                name="oldPassword"
                className="form-control"
                placeholder="Ancien Mot de Passe"
                required
                autoFocus
              />
              <label htmlFor="inputPassword" className="sr-only">Nouveau mot de passe</label>
              <input
                type="password"
                name="newPassword"
                className="form-control"
                placeholder="🔒   Nouveau Mot De passe"
                required
              />
              <label htmlFor="inputPassword" className="sr-only">Nouveau mot de passe</label>
              <input
                type="password"
                name="confirmPassword"
                className="form-control"
                placeholder="Confirmation"
                required
              />
              <button
                className="btn btn-primary modal__button-connect"
                type="submit"
              >
              Modifier
              </button>
              <button
                onClick={this.handleReturn}
                className="btn btn-secondary modal__button-return"
                type="submit"
              >
                Retour
              </button>
            </form>
           </div>
        </Modal>
      )
    }
}
