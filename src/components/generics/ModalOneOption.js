import React, { Component } from 'react'
import Modal from 'react-modal'

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    width                 : '50rem',
    height                 : '35rem',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
}

export default class ModalOneOption extends Component {
  componentDidMount = () => {
    Modal.setAppElement('body')
  }

    render = () => {
      const {
        isOpen,
        contentLabel,
        modalStyle,
        handleClickAction,
        classTitle,
        title,
        handleCloseModal,
        buttonNameAction,
        buttonNameRemoveModal,
        classContainer,
        bodyText,
        emoticon
      } = this.props

      return (
        <Modal
          isOpen={isOpen}
          onRequestClose={handleCloseModal}
          ariaHideApp={false}
          contentLabel={contentLabel}
          closeTimeoutMS={200}
          style={customStyles}
        >
          <div className="card p-5 mt-5 text-center">
  <div className="card-body">
    <h2 className='font-weight-bold mb-5'>🥺 </h2>
    <h3 className='font-weight-bold mb-5'>Vous êtes sur le point de supprimer votre compte.</h3>
    <button
    className='btn btn-danger font-weight-bold'
    onClick={handleClickAction}
    >
    Je supprime mon compte
    </button>
    <button
    className='btn btn-secondary ml-5'
    onClick={handleCloseModal}
    >
    J'ai changé d'avis
    </button>

  </div>
</div>
        </Modal>
      )
    }
}
