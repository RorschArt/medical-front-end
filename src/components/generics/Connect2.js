import React, { Component } from 'react'
import { connect } from 'react-redux'
import { startLogin } from '../../actions/loginActions'

class Connect extends Component {
  state = {
      isOpen: false
  }

  toggleModal = () => {
    this.setState((prevState) => ({
      isOpen : !prevState.isOpen
    }))
  }
  // Une arrow fonction pour avoir accés a this.
  handleSubmit = async (email, password) => {
    try {
      this.props.startLogin(email.trim(), password.trim())

    } catch (err) {
      throw new Error()
    }
  }

  render () {
    return (
      <div>
      <button
        className='button button--warning'
        onClick={this.toggleModal}>
          Connexion
      </button>
      <ModalConnect
        handleCloseModal={this.toggleModal}
        onRequestClose={this.toggleModal}
        isOpen={this.state.isOpen}
        contentLabel="connectez-vous"
        handleClickAction={this.toggleModal}
      />

      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    startLogin: async (email, password) => dispatch(startLogin(email, password))
  }
}

export default connect(null, mapDispatchToProps)(Connect)
