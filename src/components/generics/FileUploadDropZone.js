import React from 'react'

const FileUploadDropZone = (props) => {
const { classStyleDropZone, classStyleInput, handleUploadImage } = props
  return (
    <div className={classStyleDropZone}>
      Ajouter
      <input
        type="file"
        name="imageArticle"
        accept=".jpg, .png, .jpg"
        onChange={handleUploadImage}
      />
    </div>
  )
}
export default FileUploadDropZone
