import React, { Component } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'

class ModalEditMessage extends Component {
  state = {
    error: undefined
  }

  handleSubmit = (e) => {
    e.preventDefault()

    const header = e.target.elements.header.value.trim()
    const textBody = e.target.elements.textBody.value.trim()

    this.props.handleSubmit(header, textBody)
  }

  handleChangeHeader = (e) => {
    this.props.handleChangeHeader(e.target.value.trim())
  }

  handleChangeTextBody = (e) => {
    this.props.handleChangeTextBody(e.target.value.trim())
  }

  handleClickSave = (e) => {
    e.preventDefault()
    this.props.handleSave(this.props._id)
  }

  componentDidMount = () => {
    Modal.setAppElement('body')
  }


    render = () => {
      const {
        isOpen,
        contentLabel,
        modalStyle,
        classTitle,
        title,
        onRequestClose,
        handleSave,
        classContainer,
        bodyText,
        emoticon,
        loaderDisplay,
        messageSavedFocused
      } = this.props

      return (
        <Modal
          isOpen={isOpen}
          onRequestClose={onRequestClose}
          ariaHideApp={false}
          contentLabel={contentLabel}
          closeTimeoutMS={200}
          className={modalStyle}
        >
          <div className={classContainer}>
            <p>{emoticon}</p>
            <h1 className='modal__title'>{title}</h1>
            <p className='modal__body'>{bodyText}</p>
            <form onSubmit={this.handleSubmit}>
              <input
                autoFocus
                defaultValue={messageSavedFocused.header}
                placeholder='Objet'
                name='header'
                type='text'
                onChange={this.handleChangeHeader}
              />
              <div className="custom-file">
                <textarea
                defaultValue={messageSavedFocused.textBody}
                placeholder='Votre Message'
                name='textBody'
                type='text'
                className='border shadow mw-80 w-80 h-70 -mh-70'
                onChange={this.handleChangeTextBody}
                />
              </div>
              { this.state.error && <p className='error-message'>{this.state.error}</p>}
              <button className='button button--warning'>Envoyer</button>

              <button
                className='button button--link'
                onClick={this.handleClickSave}
              >
                Sauvegarder
              </button>
            </form>
          </div>
        </Modal>
      )
    }
}

const mapStateToProps = (state) => ({
  messageSavedFocused: state.filters.msgSavedFocused
})

export default connect(mapStateToProps)(ModalEditMessage)
