import React from 'react'
import uuid from 'uuid'

const ImageCard = ({
  imageId,
  classContainer,
  classImg,
  articleCard,
  avatarId,
  avatarCard
}) => {
  {
    if (articleCard) {
    return (
      <div className={classContainer} >
        <img
          className={classImg}
          src={`http://localhost:3000/articles/${imageId}/image`}
        />
      </div>
    )
  } else if (avatarCard) {
      return (
        <div className={classContainer}>
          {
            !avatarId &&
            <img
              className={classImg}
              src={`https://static.thenounproject.com/png/1454583-200.png`}
            />
          }
          {
            avatarId  &&
              <img
                className={classImg}
                src={`http://localhost:3000/users/${avatarId}/avatar/${uuid()}`}
              />
          }
        </div>
      )
    }
  }
}

export default ImageCard
