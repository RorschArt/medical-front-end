import React, { Component } from 'react'
import { connect } from 'react-redux'
import Avatar from '../users/Avatar'

class AdminSelected extends Component {

  handleClickAdmin = (adminId) => {
    this.props.setAdminIdMsg(adminId)
  }

  render() {
    const { admin, isFetching } = this.props

    return (
      <div>
      <div className="mt-5 text-center">
      <h3 className='font-weight-bold'> Professionnel selectionné :</h3>
      </div>
      <div className='d-flex justify-content-center mt-3'>
      {
        admin &&
          <Avatar
            avatarId={admin.user.avatarId}
            user={admin}
            showAvatarImage
            size={{width: 7 + 'rem'}}
          />
    }
    </div>
      </div>
    )
  }
}

const mapSateToProps = (state) => {
  return {
    admin: state.dashboard.adminSelectedMsg
  }
}

export default connect(mapSateToProps)(AdminSelected)
