import React, { Component } from 'react'
import ImageCard from '../generics/ImageCard'


class ArticleListItem extends Component {
  state = {
    isExpanded: false
  }

  handleSelectClick = (e) => {
    e.stopPropagation()
    this.props.handleSelect(this.props._id)
  }

  handleExpandClick = () => this.setState((prevState) => ({isExpanded: !prevState.isExpanded}))

  render() {
    const { title,
      subTitle,
      textBodyArticle,
      imageId,
      isExpanded,
      handleExpandClick,
      handleEditClick,
      handleDelete,
      handleSelectClick,
      articleFocused
    } = this.props

    return (
      <div
        className={this.state.isExpanded ? 'article--admin' : 'article--admin--folded'}
      >
        <button
          className='button button--information button--small'
          onClick={this.handleExpandClick}
        >
          Etendre
        </button>
        <p className='article__title--admin'>{title}</p>
        {
          imageId &&  <ImageCard
                        articleCard
                        renderImage={imageId}
                        imageId={imageId}
                        classContainer=''
                        classImg='article__image article__image--admin'
                      />
        }
        {
          this.state.isExpanded &&
            <button
              className='button button--information button--small'
              onClick={handleEditClick}
            >
              Editer
            </button>
        }
        {
          this.state.isExpanded &&
            <button
              className='button button--warning button--small'
              onClick={handleDelete}
            >
              Supprimer
            </button>
        }
        {
          this.state.isExpanded &&
            <button
              className={ articleFocused
                ? 'button button--small button--link button--selected'
                : 'button button--link button--small'}
              onClick={this.handleSelectClick}
            >
              Selectionner
            </button>
        }
      </div>
    )
  }
}

export default ArticleListItem
