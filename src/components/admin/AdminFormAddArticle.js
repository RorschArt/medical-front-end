import React from 'react'
import { connect } from 'react-redux'
import * as yup from 'yup'
import { withFormik, Form, Field, ErrorMessage } from 'formik'

const FormikForm = (props, {
  values,
  errors,
  touched,
  isSubmitting,
  title,
  subTitle,
  textBody
}) => {
    return (
    <div>
      <Form>
        <div>
          <ErrorMessage name='title' />
          <Field
            type='text'
            name='title'
            placeholder='Mon Titre'
          />
          <Field
            type='text'
            name='subTitle'
            placeholder='Mon Sous Titre'
          />
        <ErrorMessage name='textBody' />
          <Field
            component='textarea'
            name='textBodyArticle'
            placeholder='Mon Texte'
          />
        </div>
        <button
          className='button button--information'
          type='submit'
          disabled={isSubmitting}
        >
          Créer Mon Article
        </button>
      </Form>
    </div>
  )
}

const BasicForm = withFormik({
  mapPropsToValues ({ title, subTitle, textBodyArticle }) {
    return {
      title: title || '',
      subTitle: subTitle || '',
      textBody: textBodyArticle || ''
    }
  },
  validationSchema: yup.object().shape({
      title: yup.string()
        .min(3, 'Le Titre Doit Contenir au moins 3 caractères')
        .required('Le Titre est Obligatoire'),
      textBodyArticle: yup.string()
        .min(20, 'Le Texte doit contenir au Moins 20 Caractères')
        .required('Le texte est Obligatoire')
    }),
  async handleSubmit (values,
    { props,
      resetForm,
      setErrors,
      setSubmitting
    }) {
    try {
        await props.handleSubmitUpload(values.title, values.subTitle, values.textBodyArticle, values.image)
      setSubmitting(false)
    } catch (e) {
      setSubmitting(false)
      // await resetForm() TODO: DONNE UNE ERREUR EN LOG
    }
  }
})(FormikForm)

export default connect()(BasicForm)
