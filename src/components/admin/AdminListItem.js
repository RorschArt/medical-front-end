import React, { Component } from 'react'
import Avatar from '../users/Avatar'

 class AdminListItem extends Component {
   handleOnClick = () => {
    this.props.handleClick(this.props.admin)
   }

   render() {
     const { admin, isFetching } = this.props

    return (
        <div
        onClick={this.handleOnClick}
        >
        <li className="list-group-item m-3 ">
          {
            isFetching &&
            <div className="text-center">
              <div className="spinner-border" role="status">
              </div>
            </div>
          }
          <Avatar
            avatarId={admin. user.avatarId}
            user={{user: admin.user}}
            showAvatarImage
            size={this.props.sizeAvatar}
          />
        </li>
        </div>
    )
   }
}

export default AdminListItem
