import React, { Component } from 'react'
import AdminListItem from '../admin/AdminListItem'
import { connect } from 'react-redux'
import { setAdminIdMsg } from '../../actions/admin/adminActions'


class AdminList extends Component {

  handleClickAdmin = (admin) => {
    this.props.setAdminIdMsg(admin)
  }

  render() {
    const { admins, isFetching } = this.props

    return (
      <div>
      <ul className="list-group d-flex flex-row justify-content-around ">
        {
          admins && admins.map((admin) =>
                <AdminListItem
                  isFetching={isFetching}
                  key={admin._id}
                  render
                  admin={admin}
                  handleClick={this.handleClickAdmin}
                  sizeAvatar={this.props.sizeAvatar}
                />
              )
        }
      </ul>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setAdminIdMsg: async (adminId) => dispatch(setAdminIdMsg(adminId))
  }
}

export default connect(null, mapDispatchToProps)(AdminList)
