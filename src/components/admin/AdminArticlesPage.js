import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getAllArticles } from '../../actions/articlesActions'
import { setIdArticleFocused, startDeletingArticle } from '../../actions/admin/editArticlesActions'
import { setAdminArticlesListExpand } from '../../actions/filterActions'
import AdminArticleListItem from './AdminArticlesListItems'

class AdminArticlesPage extends Component {
  componentDidMount() {
      this.props.fetchAllArticles()
  }
  handlSelect = (idArticle) => {
    this.props.setArticleIdFocused(idArticle)
  }

  handleEditClick = (e) => {
    e.stopPropagation()
    console.log('edit')
  }

  handleDelete = async () => {
    await this.props.deleteArticle()
    this.props.fetchAllArticles()
  }

  render () {
    const { articles, isExpanded, articleIdFocused } = this.props

    return (
      <div>
        <div>
          {
            articles.isPending &&
              <div className="spinner-border" role="status">
              </div>
          }
          {
            articles.items.map((article, index) =>
            <AdminArticleListItem
              key={article._id}
              {...article}
              articleFocused={article._id === articleIdFocused ? true : false}
              isExpanded={isExpanded}
              handleEditClick={this.handleEditClick}
              handleSelect={this.handlSelect}
              handleDelete={this.handleDelete}
            />
          )}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
    articles: state.articles,
    isAdmin: state.user.isAdmin,
    isExpanded: state.filters.adminArticlesIsExpanded,
    articleIdFocused: state.adminArticleCreation.idArticleFocused
  })

const mapDispatchToProps = (dispatch) => ({
    fetchAllArticles: () => dispatch(getAllArticles()),
    setExpand: (bool) => dispatch(setAdminArticlesListExpand(bool)),
    setArticleIdFocused: (idArticle) => dispatch(setIdArticleFocused(idArticle)),
    deleteArticle: () => dispatch(startDeletingArticle())
})

export default connect(mapStateToProps, mapDispatchToProps)(AdminArticlesPage)
