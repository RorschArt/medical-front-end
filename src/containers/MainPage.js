import React from 'react'

const MainPage = (props) => (
    <div className="text-center mt-5 border-top">
      <div className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <div className="nav-scroller py-1 mb-2 border-bottom">
          <nav className="nav d-flex justify-content-between">
            <a className="p-2 text-muted link-clickable">💖 Santé</a>
            <a className="p-2 text-muted link-clickable">👨‍⚕️ Professionnels</a>
            <a className="p-2 text-muted link-clickable">📅 Emploi du temps</a>
            <a className="p-2 text-muted link-clickable">🕵️ Qui sommes nous ?</a>
            <a className="p-2 text-muted link-clickable">ℹ️ Aide</a>
            </nav>
        </div>
        <h5 className='display-4 font-weight-normal ml-5 border-bottom d-inline pr-5 pb-2 text-capitalize'>Accueil</h5>
        <main role="main" className="inner cover mt-5">
          <h1 className="cover-heading"></h1>
          <p className="lead">
          </p>
          <div className="col">
            <div className="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow position-relative">
              <div className="col p-4 d-flex flex-column position-static">
                <h2 className="d-inline-block p-3 font-weight-bold"> Un nouveau site web tout neuf !</h2>
                <h3 className="mb-4">Nous sommes heureux de vous présenter notre nouveau site web.</h3>
                <div className="col-auto  d-lg-block rounded">
                <img className='img-fluid border shadow-sm rounded' src='images/mainPage.jpg' />
                </div>
                  <p className='font-weight-bold mt-2'>Nous venons de franchir un nouveau pas aujourd'hui.</p>
                  <div className='text-md-left p-3'>
                  <p>
                    Nous comptons aujourd'hui 8 professionnels de la santé, grâce à votre fidélité et
                    votre confiance nous pouvons grandir afin de vous apporter un suivi efficace.
                    Cependant nous pensons que nous pouvons encore faire mieux, nous pouvons vous
                    faciliter la vie, et pour cela nous avons créer ce site.<br/>
                    Ici vous aurez la possibilité d'envoyer des messages à votre consultant et il vous
                    répondra par message qui arrivera dans votre messagerie.<br/>
                  </p>
                  <p>
                    Mais ce n'est pas tout ! Vous Aurez la possibilité de consulter toute l'actualité
                    lié à notre groupe médical, des articles également sur les nouveautés du domaine
                    de la santé.
                    Vous souhaitez consulter les disponibilités de votre consultant ? Il vous suffit
                    de cliquer sur son profil pour consulter l'agenda. Facile !
                    En plus de ça lorsque vous créez un compte vous aurez un espace personnel, ou vous
                    pouvez changer votre pseudo, votre image de profil etc ...
                    Tous les échanges sont anonyme ici.
                </p>
                <p>
  Si vous souhaitez en savoir plus veuillez consulter notre article, "démarrer chez nous" qui explique comment créer un compte et vous donnez plus de détails sur les fonctionnalités.
  Nous restons à votre disposition, n'hésitez pas à nous en parler, et à nous signaler tous problèmes, nous les corrigerons.
  Nous espérons que vous avez passé un bon été, maintenant c'est la rentrée et il y a beaucoup d'informations à vous faire passer, sur ce site web bien entendu, tout à porté de main !
                </p>
                </div>
                  <a href="/" className="stretched-link">En savoir plus +</a>
                </div>
            </div>
          </div>
        </main>
  </div>
</div>
)

export default MainPage
