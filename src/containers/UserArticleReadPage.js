import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
const url = process.env.MONGODB_DATABASE_URL

class UserArticleReadPage extends Component {
  componentDidMount() {

  }

  render() {
    const { imageId, title, subTitle, textBodyArticle } = this.props.article

    return (
      <div className="pt-4 container__page border-top rounded">
      <div className="nav-scroller py-1 mb-2 border-bottom pl-3 pr-3">
        <nav className="nav d-flex justify-content-between">
          <a className="p-2 text-muted link-clickable">💖 Santé</a>
          <NavLink
            exact
            to='/dashboard/adminList'
            className=''
          >
            <p className="p-2 text-muted link-clickable">👨‍⚕️ Professionnels</p>
          </NavLink>
          <a className="p-2 text-muted link-clickable">📅 Emploi du temps</a>
          <a className="p-2 text-muted link-clickable">🕵️ Qui sommes nous ?</a>
          <a className="p-2 text-muted link-clickable">ℹ️ Aide</a>
        </nav>
      </div>
      <h5 className='display-4 font-weight-normal ml-5 border-bottom d-inline pr-5 pb-2 text-capitalize'>Lecture article</h5>
      <div
      id='container__settings'
      className='border-left border-right d-flex flex-column align-items-center'
      >
        <div
          onClick={this.handleClickArticle}
          className="card mt-3 mb-5 shadow-sm"
        >
            <div  className="card" style={{width: 50 + 'rem'}}>
            <div className='text-center'>
            <h3 className="p-5 font-weight-bold">{title}</h3>
              </div>
              <img src={`${url}/articles/${imageId}/image`} className="card-img-top" />
              <div className="card-body">
              <div className='p-2'>
                <h4 className="font-weigh-bold">{subTitle}</h4>
                </div>
                <h5 id="font-height" className="card-title">{textBodyArticle}</h5>
              </div>
            </div>
        </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    article: state.filters.articleFocused
  }
}

export default connect(mapStateToProps)(UserArticleReadPage)
