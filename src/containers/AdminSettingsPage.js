import React from 'react'
import Avatar from '../components/users/Avatar'
import DeleteAccount from '../components/users/DeleteAccount'
import PasswordChanger from '../components/users/PasswordChanger'

const AdminSettingsPage = ({ user }) => {
  return (
    <div className=''>
      <h1>Je suis la Page Admin Settings </h1>
      <Avatar
        user={user.data.user}
        avatarId={user.data.user.avatarId}
        showAvatarImage
        needButtonUpload
      />
      <DeleteAccount/>
      <PasswordChanger />
    </div>
  )
}

export default AdminSettingsPage
