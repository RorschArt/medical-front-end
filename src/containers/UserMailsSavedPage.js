import React from 'react'
import MailsSavedList from '../components/users/mails/MailsSavedList'
import { NavLink } from 'react-router-dom'

// user est donné par le Provider (voir PrivateRoute) correspond à l'user connecté
const UseSavedMails = () => {
  return (
    <div className='p2'>
      <div className='pt-4 container__page border-top rounded'>
        <div className="nav-scroller py-1 mb-2 border-bottom pl-3 pr-3">
          <nav className="nav d-flex justify-content-between">
            <a className="p-2 text-muted link-clickable">💖 Santé</a>
            <NavLink
            exact
            to='/dashboard/adminList'
            className=''
            >
              <p className="p-2 text-muted link-clickable">👨‍⚕️ Professionnels</p>
            </NavLink>
            <a className="p-2 text-muted link-clickable">📅 Emploi du temps</a>
            <a className="p-2 text-muted link-clickable">🕵️ Qui sommes nous ?</a>
            <a className="p-2 text-muted link-clickable">ℹ️ Aide</a>
            </nav>
        </div>
      </div>
      <h5 className='display-4 font-weight-normal ml-5 border-bottom d-inline pr-5 pb-2 text-capitalize'>Messages Sauvegardés</h5>
      <MailsSavedList />
    </div>
  )
}

export default UseSavedMails
