import React, { Component } from 'react'
import SendMailForm from '../components/users/mails/SendMailForm'
import AdminList from '../components/admin/AdminList'
import AdminSelected from '../components/admin/AdminSelected'
import { NavLink } from 'react-router-dom'

export default class UserCreateMailPage extends Component {

    render() {
      return (
        <div className='p2'>
          <div className='pt-4 container__page border-top rounded'>
            <div className="nav-scroller py-1 mb-2 border-bottom pl-3 pr-3">
              <nav className="nav d-flex justify-content-between">
                <a className="p-2 text-muted link-clickable">💖 Santé</a>
                <NavLink
                exact
                to='/dashboard/adminList'
                className=''
                >
                  <p className="p-2 text-muted link-clickable">👨‍⚕️ Professionnels</p>
                </NavLink>
                <a className="p-2 text-muted link-clickable">📅 Emploi du temps</a>
                <a className="p-2 text-muted link-clickable">🕵️ Qui sommes nous ?</a>
                <a className="p-2 text-muted link-clickable">ℹ️ Aide</a>
                </nav>
            </div>
          </div>
          <h5 className='display-4 font-weight-normal ml-5 border-bottom d-inline pr-5 pb-2 text-capitalize'>rédaction message</h5>
          <div id='container__settings' className='border-left border-right'>
          <div className='font-weight-bold bg-light mb-3 p-5 text-center'>
            <h2 className='m-4'>Cliquez sur le destinataire de votre choix !</h2>
            <AdminList
              sizeAvatar={{width: 5 + 'rem'}}
              admins={this.props.dashboard.adminList}
              isFetching={this.props.dashboard.adminListPending}
            />
          </div>
          <AdminSelected />
          <SendMailForm
            history={this.props.history}
            handleSubmitClick={this.handleSubmitClick}
          />
          </div>
        </div>
      )
  }
}
