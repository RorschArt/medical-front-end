import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getAllArticles } from '../actions/articlesActions'
import { setArticleInfo } from '../actions/filterActions'
import ArticleListItem from '../components/articles/ArticleListItem'
import { NavLink } from 'react-router-dom'

class UserArticlesPage extends Component {
  ComponentDidMount() {
      this.props.fetchAllArticles()
  }

  handleClick = async (article) => {
      await this.props.setArticleInfo(article)
      this.props.history.push('/user/article/read')
  }

  render () {
    const { articles } = this.props

    return (
      <div className="pt-4 container__page border-top rounded mt-5" >
      <div className="nav-scroller py-1 mb-2 border-bottom pl-3 pr-3">
        <nav className="nav d-flex justify-content-between">
          <a className="p-2 text-muted link-clickable">💖 Santé</a>
          <NavLink
          exact
          to='/dashboard/adminList'
          className=''
          >
            <p className="p-2 text-muted link-clickable">👨‍⚕️ Professionnels</p>
          </NavLink>
          <a className="p-2 text-muted link-clickable">📅 Emploi du temps</a>
          <a className="p-2 text-muted link-clickable">🕵️ Qui sommes nous ?</a>
          <a className="p-2 text-muted link-clickable">ℹ️ Aide</a>
        </nav>
      </div>
      <h5 className='display-4 font-weight-normal ml-5 border-bottom d-inline pr-5 pb-2 text-capitalize'>Informations</h5>
        <div
        id='container__settings'
        className='border-left border-right d-flex flex-column align-items-center'
        >
          {articles.items.map((article, index) =>
            <ArticleListItem
              handleClick={this.handleClick}
              key={article._id}
              article={article}
              index={index}
            />
          )}
      </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
    articles: state.articles
  })

const mapDispatchToProps = (dispatch) => ({
    fetchAllArticles: () => dispatch(getAllArticles()),
    setArticleInfo: (article) => dispatch(setArticleInfo(article))
})

export default connect(mapStateToProps, mapDispatchToProps)(UserArticlesPage)
