import React from 'react'
import Avatar from '../components/users/Avatar'
import { NavLink } from 'react-router-dom'
import MailsReceivedList from '../components/users/mails/MailsReceivedList'
import MailsSavedList from '../components/users/mails/MailsSavedList'
import MailsUnreadCouter from '../components/users/mails/MailsUnreadCounter'
import MailsSendedList from '../components/users/mails/MailsSendedList'
import SendMailPage from '../components/users/mails/SendMailPage'

// user est donné par le Provider (voir PrivateRoute) correspond à l'user connecté
const AdminMailBoxPage = ({ user, history }) => {
  return (
    <div className='wrapper'>
      <NavLink
        exact
        to='/dashboard/sendMail'
        activeStyle={{
          textDecoration: 'none',
          color: 'red',
          fontWeight: 'bold'
        }}
        >
          Envoyer Un message
      </NavLink>
      <h1>Je suis la Page des Mail </h1>
      <Avatar
        avatarId={user.data.user.avatarId}
        user={user}
        showAvatarImage
      />
    <MailsSendedList />
    <MailsReceivedList history={history}/>
    <MailsSavedList />
    <SendMailPage />
    </div>
  )
}

export default AdminMailBoxPage
