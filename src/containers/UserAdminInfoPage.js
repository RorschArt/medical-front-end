import React, { Component } from 'react'
import AdminList from '../components/admin/AdminList'


// user est donné par le Provider (voir PrivateRoute) correspond à l'user connecté
class AdminInfoPage extends Component {

  render() {
    return (
      <div className='p2'>
        <div className='pt-4 container__page border-top rounded'>
          <div className="nav-scroller py-1 mb-2 border-bottom pl-3 pr-3">
            <nav className="nav d-flex justify-content-between">
              <a className="p-2 text-muted link-clickable">💖 Santé</a>
              <a className="p-2 text-muted link-clickable">👨‍⚕️ Professionnels</a>
              <a className="p-2 text-muted link-clickable">📅 Emploi du temps</a>
              <a className="p-2 text-muted link-clickable">🕵️ Qui sommes nous ?</a>
              <a className="p-2 text-muted link-clickable">ℹ️ Aide</a>
              </nav>
          </div>
        </div>
        <h5 className='display-4 font-weight-normal ml-5 border-bottom d-inline pr-5 pb-2 text-capitalize'>Les professionnels</h5>
        <div
          id='container__settings'
          className='border-left border-right'
        >
        <AdminList
          admins={this.props.dashboard.adminList}
          isFetching={this.props.dashboard.adminListPending}
          sizeAvatar={{width: 10 + 'rem'}}
        />
        </div>

      </div>
    )
  }

}

export default AdminInfoPage
