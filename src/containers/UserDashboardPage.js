import React from 'react'
import Avatar from '../components/users/Avatar'
import OverviewMails from '../components/users/mails/OverviewMails'
import { NavLink } from 'react-router-dom'

const UserDashboardPage = ({ user, dashboard }) => {
  return (
    <div className='pt-4 container__page border-top rounded'>
      <div className="nav-scroller py-1 mb-2 border-bottom pl-3 pr-3">
        <nav className="nav d-flex justify-content-between">
          <a className="p-2 text-muted link-clickable">💖 Santé</a>
          <NavLink
          exact
          to='/dashboard/adminList'
          className=''
          >
            <p className="p-2 text-muted link-clickable">👨‍⚕️ Professionnels</p>
          </NavLink>
          <a className="p-2 text-muted link-clickable">📅 Emploi du temps</a>
          <a className="p-2 text-muted link-clickable">🕵️ Qui sommes nous ?</a>
          <a className="p-2 text-muted link-clickable">ℹ️ Aide</a>
        </nav>
      </div>
      <h5 className='display-4 font-weight-normal ml-5 border-bottom d-inline pr-5 pb-2 text-capitalize'>Mon espace</h5>
      <div
        id='container__settings'
        className='border-left border-right'
      >
        <div className='mb-5 ml-5 d-flex justify-content-between'>
            <Avatar
              avatarId={dashboard.avatarId}
              user={user}
              size={{width: 7 + 'rem'}}
          />
          <OverviewMails />
        </div>
      </div>
    </div>
  )
}

export default UserDashboardPage
