import React, { Component } from 'react'
import { connect } from 'react-redux'
import FormAddArticle from '../components/admin/AdminFormAddArticle'
import FileUploadDropZone from '../components/generics/FileUploadDropZone'
import { getAllArticles } from '../actions/articlesActions'
import {
  startUploadContentArticle,
  setIdImageArticleInStore,
  startUploadImageArticle
} from '../actions/admin/createArticleAction'

class AdminCreatePage extends Component {

  handleCreateArticle = (title, subTitle, textBody, image) => {
      const { startCreateArticle, history, fetchAllArticles } = this.props

      startCreateArticle(title.trim(), subTitle.trim(), textBody.trim())
      history.push('/dashboard/admin/CreateSucceed')
    }

    handleUploadImage = (e) => {
      const { startUploadImageArticle } = this.props
      const imageFile = e.target.files[0]
      startUploadImageArticle(imageFile)
    }

  render () {
    const { contentArticleIsUploading, imageArticleisUploading, imageUploaded } = this.props

    return (
      <div className='wrapper create-article'>
        <h1>Je suis la page D'ajout d'article</h1>
        <FormAddArticle
          addArticleForm
          handleSubmitUpload={this.handleCreateArticle}
          TriggerImageUpload={this.handleUploadImage}
        />
      {
        <FileUploadDropZone
          handleUploadImage={this.handleUploadImage}
          classStyleDropZone={
                        imageUploaded ?
                          'drop-zone drop-zone--upload-completed drop-zone--medium' :
                          'drop-zone drop-zone--medium'
                      }
          classStyleInput='drop-Zone-input'
        />
      }
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  imageArticleisUploading : state.adminArticleCreation.imageArticleisUploading,
  contentArticleIsUploading: state.adminArticleCreation.contentArticleIsUploading,
  imageUploaded: state.adminArticleCreation.imageUploaded
})

const mapDispatchToProps = (dispatch) => ({
  fetchAllArticles: () => dispatch(getAllArticles()),
  startCreateArticle: (title, subTitle, textBody, file) => dispatch(startUploadContentArticle(title, subTitle, textBody, file)),
  setIdImageArticleInStore: (image) => dispatch(setIdImageArticleInStore(image)),
  startUploadImageArticle: (imageFile) => dispatch(startUploadImageArticle(imageFile))
})

export default connect(mapStateToProps, mapDispatchToProps)(AdminCreatePage)
