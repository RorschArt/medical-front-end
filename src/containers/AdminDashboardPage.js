import React from 'react'
import Avatar from '../components/users/Avatar'
import AdminArticlePage from '../components/admin/AdminArticlesPage'

const AdminDashboardPage = ({ user }) => {
  return (
    <div className='wrapper'>
      <h1>Je suis le dashboard Admin </h1>
      <Avatar
        avatarId={user.data.user.avatarId}
        user={user.data.user}
        showAvatarImage
      />
      <AdminArticlePage />
    </div>
  )
}

export default AdminDashboardPage
