import axios from '../middlewares/axiosConfig'
export const SET_REQUEST_USERS_LIST = 'SET_REQUEST_USERS_LIST'
export const SET_INVALIDATE_USERS_LIST = 'SET_INVALIDATE_USERS_LIST'
export const SET_INFOS_USERS_LIST_RECEIVED = 'SET_INFOS_USERS_LIST_RECEIVED'

const setInfosResultUsersList = (usersList) => ({
  type: SET_INFOS_USERS_LIST_RECEIVED,
  usersList
})

const setInvalidateUsersList = () => ({
  type: SET_INVALIDATE_USERS_LIST
})

const setRequestUsersList = () => ({
  type: SET_REQUEST_USERS_LIST
})

export const startSearchingUser = () => async (dispatch, getState) => {
  try {
    dispatch(setRequestUsersList())
    //TODO: integrer le token dans axios (middleware) pour refactoriser
    const state = getState()
    const pseudoName = state.filters.pseudoNameFilter

    const bodyRequest = {
      pseudoName
    }

    const response = await axios.get(`/users/find/${pseudoName}`, bodyRequest)

     dispatch(setInfosResultUsersList(response.data))

  } catch(err) {
    dispatch(setInvalidateUsersList())
  }
}
