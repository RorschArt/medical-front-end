export const SET_PSEUDONAME_FILTER_USERS_LIST = 'SET_PSEUDONAME_FILTER_USERS_LIST'
export const SET_DISPLAY_AVATAR_FILTER = 'SET_DISPLAY_AVATAR_FILTER'
export const SET_DISPLAY_USERS_LIST_FILTER = 'SET_DISPLAY_USERS_LIST_FILTER'
export const SET_DISPLAY_SAVE_MSG_COMPONENT = 'SET_DISPLAY_SAVE_MSG_COMPONENT'
export const SET_DISPLAY_SEND_MSG_COMPONENT = 'SET_DISPLAY_SEND_MSG_COMPONENT'
export const SET_DISPLAY_RECEIVE_MSG_COMPONENT = 'SET_DISPLAY_RECEIVE_MSG_COMPONENT'
export const SET_DISPLAY_USER_PREVIEW = 'SET_DISPLAY_USER_PREVIEW'
export const SET_ADMIN_ARTICLES_EXPANDED = 'SET_ADMIN_ARTICLES_EXPANDED'
export const SET_MSG_RECEIVED_FOCUSED = 'SET_MSG_RECEIVED_FOCUSED'
export const SET_MSG_SAVED_FOCUSED = 'SET_MSG_SAVED_FOCUSED'
export const SET_MSG_SENDED_FOCUSED = 'SET_MSG_SENDED_FOCUSED'
export const SET_MSG_SAVED_DELETED_FOCUSED = 'SET_MSG_SAVED_DELETED_FOCUSED'
export const SET_MSG_RECEIVED_DELETED_FOCUSED = 'SET_MSG_RECEIVED_DELETED_FOCUSED'
export const SET_MSG_SENDED_DELETED_FOCUSED = 'SET_MSG_SENDED_DELETED_FOCUSED'
export const SET_USER_ID_FOCUSED = 'SET_USER_ID_FOCUSED'
export const SET_USER_INFOS_FOCUSED = 'SET_USER_INFOS_FOCUSED'
export const SET_DELETE_INFOS_USER_FOCUSED = 'SET_DELETE_INFOS_USER_FOCUSED'
export const SET_DELETE_ID_USER_FOCUSED = 'SET_DELETE_ID_USER_FOCUSED'
export const SET_INFOS_ARTICLE = 'SET_INFOS_ARTICLE'

export const setPseudoNameUsersListFilter = (pseudoName) => ({
  type: SET_PSEUDONAME_FILTER_USERS_LIST,
  pseudoName
})

export const setUserIdFocused = (userId) => ({
  type: SET_USER_ID_FOCUSED,
  userId
})

export const setUserInfosFocused = (user) => ({
  type: SET_USER_INFOS_FOCUSED,
  user
})

export const deleteUserInfosFocused = () => ({
  type: SET_DELETE_INFOS_USER_FOCUSED
})

export const deleteUserIdFocused = () => ({
  type: SET_DELETE_ID_USER_FOCUSED
})

export const setDisplayAvatar = (displayAvatar) => ({
  type: SET_DISPLAY_AVATAR_FILTER,
  displayAvatar
})

export const setDisplayUsersList = (displayUsersList) => ({
  type: SET_DISPLAY_USERS_LIST_FILTER,
  displayUsersList
})

export const setSendingMailCompVisibility = (sendEmailCompVisible) => ({
  type: SET_DISPLAY_SEND_MSG_COMPONENT,
  sendEmailCompVisible
})

export const setUserPreviewFocusedVisibility = (UserPreviewVisible) => ({
  type: SET_DISPLAY_USER_PREVIEW,
  UserPreviewVisible
})

export const setReceivingMailCompVisibility = (receiveEmailCompVisible) => ({
  type: SET_DISPLAY_RECEIVE_MSG_COMPONENT,
  receiveEmailCompVisible
})

export const setSavingMailCompVisibility = (saveEmailCompVisible) => ({
  type: SET_DISPLAY_SAVE_MSG_COMPONENT,
  saveEmailCompVisible
})

export const setMsgReceivedFocused = (msg) => ({
  type: SET_MSG_RECEIVED_FOCUSED,
  msg
})

export const setMsgSendedFocused = (msg) => ({
  type: SET_MSG_SENDED_FOCUSED,
  msg
})

export const setMsgSavedFocused = (msg) => ({
  type: SET_MSG_SAVED_FOCUSED,
  msg
})

export const setMsgSavedDeletedFocused = (msg) => ({
  type: SET_MSG_SAVED_DELETED_FOCUSED,
  msg
})

export const setMsgReceivedDeletedFocused = (msg) => ({
  type: SET_MSG_RECEIVED_DELETED_FOCUSED,
  msg
})

export const setMsgSendedDeletedFocused = (msg) => ({
  type: SET_MSG_SENDED_DELETED_FOCUSED,
  msg
})

export const setAdminArticlesListExpand = (isExpanded) => ({
  type: SET_ADMIN_ARTICLES_EXPANDED,
  isExpanded
})

export const setArticleInfo = (article) => ({
  type: SET_INFOS_ARTICLE,
  article
})
