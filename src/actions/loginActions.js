import axios from '../middlewares/axiosConfig'
import { toast } from 'react-toastify'
import { setAvatarId, } from './users/dashboardAction'
// OBLIGATOIRE Pour Jest car test des String
export const SET_USER_INFOS = 'SET_USER_INFOS'
export const SET_LOGIN_PENDING = 'SET_LOGIN_PENDING'
export const SET_IS_ADMIN = 'SET_IS_ADMIN'
export const SET_LOGOUT_PENDING = 'SET_LOGOUT_PENDING'
export const SET_IS_ADMIN_TO_FALSE = 'SET_IS_ADMIN_TO_FALSE'
export const SET_LOGIN_HAS_FAILED = 'SET_LOGIN_HAS_FAILED'
export const SET_IS_AUTHENTICATED = 'SET_IS_AUTHENTICATED'
export const SET_LOGOUT_HAS_FAILED = 'SET_LOGOUT_HAS_FAILED'
export const SET_IS_NOT_AUTHENTICATED = 'SET_IS_NOT_AUTHENTICATED'
export const SET_PASSWORD_CHECKING_PENDING = 'SET_PASSWORD_CHECKING_PENDING'
export const SET_PASSWORD_CHECKING_INVALIDATE = 'SET_PASSWORD_CHECKING_INVALIDATE'
export const SET_PASSWORD_CHECKING_SUCCESS = 'SET_PASSWORD_CHECKING_SUCCESS'


export const loginNotificationSetUp = {
  position: 'bottom-right',
  autoClose: 4000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true

}

export const setUserInfo = (token, data) => ({
  type: SET_USER_INFOS,
  token,
  data,
  lastConnection: new Date()
})

export const setIsAuthenticated = () => ({
  type: SET_IS_AUTHENTICATED,
  lastConnection: Date.now()
})
export const setIsNotAuthenticated = () => ({
  type: SET_IS_NOT_AUTHENTICATED,
  lastDisconnection: Date.now()
})

export const setIsAdmin = (isAdmin) => ({
  type: SET_IS_ADMIN,
  isAdmin
})

export const setLoginPending = (setLoginPending) => ({
  type: SET_LOGIN_PENDING,
  setLoginPending
})
const setLoginHasFailed = () => ({
  type: SET_LOGIN_HAS_FAILED
})

const setLogoutPending = (isLogoutPending) => ({
  type: SET_LOGOUT_PENDING,
  isLogoutPending
})

const setLogoutHasFailed = (logoutHasFailed) => ({
  type: SET_LOGOUT_HAS_FAILED,
  logoutHasFailed
})

const setPasswordCheckingPending = (checkingPending) => ({
  type: SET_PASSWORD_CHECKING_PENDING,
  checkingPending
})

export const setPasswordCheckingInvalidate = () => ({
  type: SET_PASSWORD_CHECKING_INVALIDATE
})

export const setPasswordCheckingSucceed = () => ({
  type: SET_PASSWORD_CHECKING_SUCCESS
})

export const startLogin = (email, password) => async (dispatch) => {
  try {
    dispatch(setLoginPending(true))
    // TODO: Obliger de mettre await (mauvaise implémentation, à revoir)
    await dispatch(startCheckingPassword(email, password))

    toast.success('🧘 Connexion réussie !', loginNotificationSetUp)
  } catch (err) {
    dispatch(setLoginHasFailed(true))

    toast.error('🔍 Connexion impossible', loginNotificationSetUp)
    // Pour le try/catch du Form Component
    throw new Error()
  }
}

export const startLogout = () => async (dispatch) => {
  try {

    dispatch(setLogoutPending(true))

    const response = await axios.post('/users/logout')

    if (response.status === 200) {
      dispatch(setIsNotAuthenticated())
      toast.info('🧘 Vous êtes déconnecté.', loginNotificationSetUp)
    }

  } catch (err) {
      dispatch(setLogoutHasFailed(true))
      toast.error('🔍 Une erreur est survenue.', loginNotificationSetUp)
  }
}

export const startCheckingPassword = (email, password) => async (dispatch) => {
  try {
    dispatch(setPasswordCheckingPending())

    const bodyRequest = {
      email,
      password
    }

    const response = await axios.post('/users/login', bodyRequest)
    const { user, token } = response.data

    if (user.roleName === 'admin') {
      dispatch(setIsAdmin(true))
    }

    //TODO: Back end a revoir User nested Dans l'entité role(travail long)
    dispatch(setAvatarId(user.user.avatarId || null))
    dispatch(setUserInfo(token, user))
    dispatch(setPasswordCheckingSucceed())

  } catch(err) {
      dispatch(setPasswordCheckingInvalidate())
      throw new Error('check password has failed')
  }
}
