import axios from '../../middlewares/axiosConfig'
import { toast } from 'react-toastify'
export const SET_SUCCESS_UPLOAD_ARTICLE = 'SET_SUCCESS_UPLOAD_ARTICLE'
export const SET_SUCCESS_UPLOAD_IMAGE_ARTICLE = 'SET_SUCCESS_UPLOAD_IMAGE_ARTICLE'
export const SET_INVALIDATE_UPLOAD_ARTICLE = 'SET_INVALIDATE_UPLOAD_ARTICLE'
export const SET_PENDING_UPLOAD_ARTICLE = 'SET_PENDING_UPLOAD_ARTICLES'
export const SET_INVALIDATE_UPLOAD_IMAGE_ARTICLE = 'SET_INVALIDATE_UPLOAD_IMAGE_ARTICLES'
export const SAVE_ID_IMAGE_ARTICLE_CREATED_IN_STORE = 'SAVE_ID_IMAGE_ARTICLE_CREATED_IN_STORES'
export const SET_PENDING_UPLOAD_IMAGE_ARTICLE = 'SET_PENDING_UPLOAD_IMAGE_ARTICLES'
export const SET_INVALIDATE_ID_IMAGE_ARTICLE_CREATED_IN_STORE = 'SET_INVALIDATE_ID_IMAGE_ARTICLE_CREATED_IN_STORES'

export const createArticleNotificationSetUp = {
  position: 'bottom-right',
  autoClose: 3000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true
}

// CONTENT ARTICLE
const setSuccessUploadArticle = (article) => ({
  type: SET_SUCCESS_UPLOAD_ARTICLE,
  article
})

const setInvalidateUploadArticle = () => ({
  type: SET_INVALIDATE_UPLOAD_ARTICLE
})

const setRequestUploadArticle = () => ({
  type: SET_PENDING_UPLOAD_ARTICLE
})
// IMAGE ARTICLE
const setSuccessUploadImageArticle = (article) => ({
  type: SET_SUCCESS_UPLOAD_IMAGE_ARTICLE,
  article
})

const setInvalidateUploadImageArticle = () => ({
  type: SET_INVALIDATE_UPLOAD_IMAGE_ARTICLE
})

const setRequestUploadImageArticle = () => ({
  type: SET_PENDING_UPLOAD_IMAGE_ARTICLE
})
// ID ARTICLE
const saveIdArticleCreated = (idImageArticleCreated) => ({
  type: SAVE_ID_IMAGE_ARTICLE_CREATED_IN_STORE,
  idImageArticleCreated
})
const setInvalidateStoreIdImageArticle = (idImageArticleCreated) => ({
  type: SET_INVALIDATE_ID_IMAGE_ARTICLE_CREATED_IN_STORE,
  idImageArticleCreated
})

export const startUploadContentArticle = (title, subTitle, textBodyArticle) => async (dispatch, getState) => {
  try {
    //TODO: integrer le token dans axios (middleware) pour refactoriser
    const state = getState()
    const token = state.user.token
    const imageId = state.adminArticleCreation.content.idImageArticleCreated

    if(!state.user.isAdmin) {
      throw new Error()
    }

    dispatch(setRequestUploadArticle())
    const bodyRequest = {
      title,
      subTitle,
      textBodyArticle,
      imageId
    }

    const article = await axios({
      method: 'post',
      url: `${process.env.MONGODB_DATABASE_URL}/articles`,
      data: bodyRequest,
      headers: {
        ContentType: 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
    dispatch(setSuccessUploadArticle(article.data))
    toast.success('📄 Ton Article a été Créer !', createArticleNotificationSetUp)

  } catch (err) {
    dispatch(setInvalidateUploadImageArticle())
    dispatch(setInvalidateUploadArticle())
    toast.error('📄 Une Erreur s\'est Produite.', createArticleNotificationSetUp)
  }
}

// Cette fonction serait utile mais impossible de mettre un formData ou encore un
// e.target.files[0] dans un state redux, le champs est vide
export const setIdImageArticleInStore = (idImageArticleCreated) => (dispatch) => {
  try {
    dispatch(saveIdArticleCreated(idImageArticleCreated))

  } catch (err) {
    dispatch(setInvalidateStoreIdImageArticle())
    toast.error('📄 Une Erreur s\'est produite, re-uploader l\'image.', createArticleNotificationSetUp)
  }
}

export const startUploadImageArticle = (imageFile) => async (dispatch, getState) => {
  try {
    dispatch(setRequestUploadImageArticle())

    const store = getState()
    const token = store.user.token

    const data  = new FormData()
    data.append('imageArticle', imageFile)

    const imageArticleResponse = await axios.post(`${process.env.MONGODB_DATABASE_URL}/articles/image`, data, {
      headers: { Authorization: `Bearer ${token}`}
    })

    dispatch(setSuccessUploadImageArticle())
    dispatch(setIdImageArticleInStore(imageArticleResponse.data._id))
    toast.success('📄 Image Uploadé.', createArticleNotificationSetUp)

  } catch (err) {
    dispatch(setInvalidateUploadImageArticle())
    toast.error('📄 Impossible d\'uploader l\'image.', createArticleNotificationSetUp)
  }
}
