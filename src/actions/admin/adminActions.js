import axios from 'axios'
export const SET_INFOS_ADMINS_RECEIVED = 'SET_INFOS_ADMINS_RECEIVED'
export const SET_INVALIDATE_ADMINS_LIST = 'SET_INVALIDATE_ADMINS_LIST'
export const SET_REQUEST_ADMINS_LIST = 'SET_REQUEST_ADMINS_LIST'
export const SET_ADMIN_INFOS_MSG = 'SET_ADMIN_INFOS_MSG'

const setAdminsReceivedInfo = (adminList) => ({
  type: SET_INFOS_ADMINS_RECEIVED,
  adminList
})

const setInvalidateAdminList = () => ({
  type: SET_INVALIDATE_ADMINS_LIST
})

const setRequestAdminList = () => ({
  type: SET_REQUEST_ADMINS_LIST
})

export const setAdminIdMsg = (admin) => ({
  type: SET_ADMIN_INFOS_MSG,
  admin
})

export const getAllAdmins = () => async (dispatch) => {
  try {
    dispatch(setRequestAdminList())

    const adminList = await axios.get(`${process.env.MONGODB_DATABASE_URL}/roles/admins/admin`)

    dispatch(setAdminsReceivedInfo(adminList.data))
  } catch (e) {
    dispatch(setInvalidateAdminList())
  }
}
