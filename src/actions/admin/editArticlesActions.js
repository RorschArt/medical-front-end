import axios from '../../middlewares/axiosConfig'
export const SET_INVALIDATE_EDITING_ARTICLE = 'SET_INVALIDATE_EDITING_ARTICLE'
export const SET_INVALIDATE_EDITING_IMAGE_ARTICLE = 'SET_INVALIDATE_EDITING_IMAGE_ARTICLE'
export const SET_PENDING_EDITING_ARTICLES = 'SET_PENDING_EDITING_ARTICLES'
export const SET_SUCCESS_EDITING_ARTICLE = 'SET_SUCCESS_EDITING_ARTICLE'
export const SET_ID_ARTICLE_FOCUSED = 'SET_ID_ARTICLE_FOCUSED'
export const SET_SUCCESS_DELETING_ARTICLE = 'SET_SUCCESS_DELETING_ARTICLE'
export const SET_INVALIDATE_DELETING_ARTICLE = 'SET_INVALIDATE_DELETING_ARTICLE'
export const SET_PENDING_DELETING_ARTICLE = 'SET_PENDING_DELETING_ARTICLE'

const setSuccessEditingArticle = (article) => ({
  type: SET_SUCCESS_EDITING_ARTICLE,
  article
})

const setPendingEditingArticle = () => ({
  type: SET_PENDING_EDITING_ARTICLES
})

const setInvalidateEditingArticle = () => ({
  type: SET_INVALIDATE_EDITING_ARTICLE
})

const setInvalidateEditingImageArticle = () => ({
  type: SET_INVALIDATE_EDITING_IMAGE_ARTICLE
})

const setPendingDeletingArticle = () => ({
  type: SET_PENDING_DELETING_ARTICLE
})

const setSuccessDeletingArticle = (article) => ({
  type: SET_SUCCESS_DELETING_ARTICLE,
  article
})

const setInvalidateDeletingArticle = () => ({
  type: SET_INVALIDATE_DELETING_ARTICLE
})

export const setIdArticleFocused = (idArticle) => ({
  type: SET_ID_ARTICLE_FOCUSED,
  idArticle
})

export const startEditingArticle = (title, subTitle, textBodyArticle, _id) => async (dispatch, state) => {
  try {
    dispatch(setPendingEditingArticle())
    const bodyRequest = {
      title,
      subTitle,
      textBodyArticle
    }

    const article = await axios.patch(`/articles/${_id}`, bodyRequest)

    dispatch(setSuccessEditingArticle(article))
  } catch (err) {
    dispatch(setInvalidateEditingArticle())
  }
}

export const startDeletingArticle = () => async (dispatch, getState) => {
  try {
    const state = getState()
    const idArticle = state.adminArticleCreation.idArticleFocused

    const article = await axios.delete(`/articles/${idArticle}`)

    dispatch(setSuccessDeletingArticle(article.data))
  } catch (err) {
    dispatch(setInvalidateDeletingArticle())
  }
}
