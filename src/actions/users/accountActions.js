import axios from 'axios'
import { toast } from 'react-toastify'
import { saveInfosWhenTryingToCreateAccount } from '../accountCreationActions'
import { startDeleteDashboardInfos } from './dashboardAction'
import { startCheckingPassword } from '../loginActions'
export const SET_DELETE_ACCOUNT_PENDING = 'SET_DELETE_ACCOUNT_PENDING'
export const SET_DELETE_ACCOUNT = 'SET_DELETE_ACCOUNT'
export const SET_DELETE_ACCOUNT_INVALIDATE = 'SET_DELETE_ACCOUNT_INVALIDATE'
export const SET_PASSWORD_RESET_PENDING = 'SET_PASSWORD_RESET_PENDING'
export const SET_PASSWORD_RESET_INVALIDATE = 'SET_PASSWORD_RESET_INVALIDATE'
export const SET_PASSWORD_RESET_SUCCESS = 'SET_PASSWORD_RESET_SUCCESS'
export const SET_DISPLAY_MODAL_RESET_PASSWORD = 'SET_DISPLAY_MODAL_RESET_PASSWORD'


export const accountNotificationSetUp = {
  position: 'bottom-right',
  autoClose: 3000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true
}

const setDeleteAccountPending = () => ({
  type: SET_DELETE_ACCOUNT_PENDING
})

const setDeleteAccountInvalidate = () => ({
  type: SET_DELETE_ACCOUNT_INVALIDATE
})

const setDeleteAccountSuccess = () => ({
  type: SET_DELETE_ACCOUNT
})


export const setResetPasswordPending = () => ({
  type: SET_PASSWORD_RESET_PENDING
})

export const setResetPasswordSuccess = () => ({
  type: SET_PASSWORD_RESET_SUCCESS
})

export const setResetPasswordInvalidate = () => ({
  type: SET_PASSWORD_RESET_INVALIDATE
})

export const showModalResetPassword = (displayModal) => ({
  type: SET_DISPLAY_MODAL_RESET_PASSWORD,
  displayModal
})

export const startDeleteAccount = () => async (dispatch, getState) => {
  try {
    const state = getState()
    const token = state.user.token

    dispatch(setDeleteAccountPending())
    dispatch(startDeleteDashboardInfos())

    const accountDeleted = await axios.delete(`${process.env.MONGODB_DATABASE_URL}/users/me`,
      { headers: { Authorization: `Bearer ${token}` } }
    )
    const { user } = accountDeleted.data
    dispatch(saveInfosWhenTryingToCreateAccount(user.email, user.password, user.pseudoName))
    dispatch(setDeleteAccountSuccess())
  } catch (err) {

    dispatch(setDeleteAccountInvalidate())
    toast.error('🗑️ Une erreur s\'est produite lors de la suppression.', accountNotificationSetUp)
  }

}

export const startResetPassword = (
  oldPassword,
  newPassword,
  confirmPassword
) => async (dispatch, getState) => {
  try {
    if(newPassword !== confirmPassword) {
      return toast.error('🗑️ Mot de passe de Confirmation différent.', accountNotificationSetUp)
    }

    const state = getState()
    const { user } = state

    // TODO: Mauvaise implémentation, un await dans un dispatch ?
    await dispatch(startCheckingPassword(user.data.user.email, oldPassword))
    dispatch(setResetPasswordPending())

      const bodyRequest = { password: newPassword }

      await axios({
        method: 'patch',
        url: `${process.env.MONGODB_DATABASE_URL}/users/me`,
        data: bodyRequest,
        headers: {
          Authorization: `Bearer ${user.token}`
        }
      })
      dispatch(setResetPasswordSuccess())
      toast.success('🔑 Mise à jour mot de passe réussite', accountNotificationSetUp)

  } catch (err) {
    if (err.message === 'check password has failed') {
      dispatch(setResetPasswordInvalidate())
      return toast.error('🙉 Mauvais ancien mot de passe.', accountNotificationSetUp)
    }

    dispatch(setResetPasswordInvalidate())
    toast.error('🗝️ Mauvais format de mot de passe', accountNotificationSetUp)
  }
}
