import axios from '../../../middlewares/axiosConfig'
import { toast } from 'react-toastify'
import {
  setMsgSavedFocused,
  setMsgSendedFocused,
  setMsgSavedDeletedFocused,
  setMsgReceivedDeletedFocused,
  setMsgSendedDeletedFocused
} from '../../filterActions'

export const SET_PENDING_SEND_MSG = 'SET_PENDING_SEND_MSG'
export const SET_SUCCESS_SEND_MSG = 'SET_SUCCESS_SEND_MSG'
export const SET_INVALIDATE_SEND_MSG = 'SET_INVALIDATE_SEND_MSG'
export const SET_PENDING_SAVING_MSG = 'SET_PENDING_SAVING_MSG'
export const SET_SUCCESS_SAVE_MSG = 'SET_SUCCESS_SAVE_MSG'
export const SET_INVALIDATE_SAVE_MSG = 'SET_INVALIDATE_SAVE_MSG'
export const SET_CONTENT_MSG_TO_SEND = 'SET_CONTENT_MSG_TO_SEND'
// Message reçus
export const SET_PENDING_FETCH_RECEIVED_MSG = 'SET_PENDING_FETCH_RECEIVED_MSG'
export const SET_SUCCESS_FETCH_RECEIVED_MSG = 'SET_SUCCESS_FETCH_RECEIVED_MSG'
export const SET_SUCCESS_MSG_RECEIVED_COUNTER = 'SET_SUCCESS_MSG_RECEIVED_COUNTER'
export const SET_INVALIDATE_FETCH_RECEIVED_MSG = 'SET_INVALIDATE_FETCH_RECEIVED_MSG'
export const SET_PENDING_DELETE_RECEIVED_MSG = 'SET_PENDING_DELETE_RECEIVED_MSG'
export const SET_INVALIDATE_DELETE_RECEIVED_MSG = 'SET_INVALIDATE_DELETE_RECEIVED_MSG'
export const SET_SUCCESS_DELETE_RECEIVED_MSG = 'SET_SUCCESS_DELETE_RECEIVED_MSG'
export const SET_PENDING_DELETE_ALL_RECEIVED_MSG = 'SET_PENDING_DELETE_ALL_RECEIVED_MSG'
export const SET_SUCCESS_DELETE_ALL_RECEIVED_MSG = 'SET_SUCCESS_DELETE_ALL_RECEIVED_MSG'
export const SET_INVALIDATE_DELETE_ALL_RECEIVED_MSG = 'SET_INVALIDATE_DELETE_ALL_RECEIVED_MSG'
// Message sauvegardés
export const SET_PENDING_FETCH_SAVED_MSG = 'SET_PENDING_FETCH_SAVED_MSG'
export const SET_SUCCESS_FETCH_SAVED_MSG = 'SET_SUCCESS_FETCH_SAVED_MSG'
export const SET_INVALIDATE_FETCH_SAVED_MSG = 'SET_INVALIDATE_FETCH_SAVED_MSG'
export const SET_PENDING_DELETE_SAVED_MSG = 'SET_PENDING_DELETE_SAVED_MSG'
export const SET_INVALIDATE_DELETE_SAVED_MSG = 'SET_INVALIDATE_DELETE_SAVED_MSG'
export const SET_SUCCESS_DELETE_SAVED_MSG = 'SET_SUCCESS_DELETE_SAVED_MSG'
export const SET_PENDING_EDIT_MSG_SAVED = 'SET_PENDING_EDIT_MSG_SAVED'
export const SET_SUCCESS_EDIT_MSG_SAVED = 'SET_SUCCESS_EDIT_MSG_SAVED'
export const SET_INVALIDATE_EDIT_MSG_SAVED = 'SET_INVALIDATE_EDIT_MSG_SAVED'
export const SET_SUCCESS_DELETE_ALL_SAVED_MSG = 'SET_SUCCESS_DELETE_ALL_SAVED_MSG'
export const SET_INVALIDATE_DELETE_ALL_SAVED_MSG = 'SET_INVALIDATE_DELETE_ALL_SAVED_MSG'
export const SET_PENDING_DELETE_ALL_SAVED_MSG = 'SET_PENDING_DELETE_ALL_SAVED_MSG'
export const SET_SUCCESS_MSG_SAVED_COUNTER = 'SET_SUCCESS_MSG_SAVED_COUNTER'
// Message envoyés
export const SET_PENDING_FETCH_SENDED_MSG = 'SET_PENDING_FETCH_SENDED_MSG'
export const SET_SUCCESS_FETCH_SENDED_MSG = 'SET_SUCCESS_FETCH_SENDED_MSG'
export const SET_INVALIDATE_FETCH_SENDED_MSG = 'SET_INVALIDATE_FETCH_SENDED_MSG'
export const SET_PENDING_DELETE_SENDED_MSG = 'SET_PENDING_DELETE_SENDED_MSG'
export const SET_INVALIDATE_DELETE_SENDED_MSG = 'SET_INVALIDATE_DELETE_SENDED_MSG'
export const SET_SUCCESS_DELETE_SENDED_MSG = 'SET_SUCCESS_DELETE_SENDED_MSG'
export const SET_SUCCESS_DELETE_ALL_SENDED_MSG = 'SET_SUCCESS_DELETE_ALL_SENDED_MSG'
export const SET_INVALIDATE_DELETE_ALL_SENDED_MSG = 'SET_INVALIDATE_DELETE_ALL_SENDED_MSG'
export const SET_PENDING_DELETE_ALL_SENDED_MSG = 'SET_PENDING_DELETE_ALL_SENDED_MSG'
export const SET_SUCCESS_MSG_SENDED_COUNTER = 'SET_SUCCESS_MSG_SENDED_COUNTER'
// Message Unread
export const SET_PENDING_MSG_UNREAD_COUNTER = 'SET_PENDING_MSG_UNREAD_COUNTER'
export const SET_SUCCESS_MSG_UNREAD_COUNTER = 'SET_SUCCESS_MSG_UNREAD_COUNTER'
export const SET_INVALIDATE_MSG_UNREAD_COUNTER = 'SET_INVALIDATE_MSG_UNREAD_COUNTER'
export const SET_SUCCESS_MSG_UNREAD = 'SET_SUCCESS_MSG_UNREAD'
export const SET_INVALIDATE_MSG_UNREAD = 'SET_INVALIDATE_MSG_UNREAD'
export const SET_PENDING_MSG_UNREAD = 'SET_PENDING_MSG_UNREAD'
export const SET_PENDING_MSG_RECEIVED_COUNTER = 'SET_PENDING_MSG_RECEIVED_COUNTER'

export const mailNotificationSetUp = {
  position: 'bottom-right',
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true
}

// SAVED
const setPendingSavingMessage = () => ({
  type: SET_PENDING_SAVING_MSG
})

const setSuccesMailSaved = () => ({
  type: SET_SUCCESS_SAVE_MSG
})

const setInvalidateMailSaving = () => ({
  type: SET_INVALIDATE_SAVE_MSG
})

const setInvalidateFetchMailsSaved = () => ({
  type: SET_INVALIDATE_FETCH_SAVED_MSG
})

const setSuccessFetchMailsSaved = (messages) => ({
  type: SET_SUCCESS_FETCH_SAVED_MSG,
  messages
})

const setPendingFetchMailsSaved = () => ({
  type: SET_PENDING_FETCH_SAVED_MSG
})
// Delete One
const setPendingDeletingMailSaved = () => ({
  type: SET_PENDING_DELETE_SAVED_MSG
})

const setInvalidateDeletingMailSaved = () => ({
  type: SET_INVALIDATE_DELETE_SAVED_MSG
})

const setSuccessDeletingMailSaved = () => ({
  type: SET_SUCCESS_DELETE_SAVED_MSG
})
// Delete All
const setSuccessDeletingAllMailsSaved = () => ({
  type: SET_SUCCESS_DELETE_ALL_SAVED_MSG
})

const setInvalidateDeletingAllMailsSaved = () => ({
  type: SET_INVALIDATE_DELETE_ALL_SAVED_MSG
})

const setPendingDeletingAllMailsSaved = () => ({
  type: SET_PENDING_DELETE_ALL_SAVED_MSG
})
// RECEIVED
const setInvalidateFetchMailsReceived = () => ({
  type: SET_INVALIDATE_FETCH_RECEIVED_MSG
})

const setSuccessFetchMailsReceived = (messages) => ({
  type: SET_SUCCESS_FETCH_RECEIVED_MSG,
  messages
})

const setPendingFetchMailsReceiving = () => ({
  type: SET_PENDING_FETCH_RECEIVED_MSG
})
// Delete One
const setPendingDeletingMailReceived = () => ({
  type: SET_PENDING_DELETE_RECEIVED_MSG
})

const setInvalidateDeletingMailReceived = () => ({
  type: SET_INVALIDATE_DELETE_RECEIVED_MSG
})

const setSuccessDeletingMailReceived = () => ({
  type: SET_SUCCESS_DELETE_RECEIVED_MSG
})
// Delete All

const setPendingDeletingAllMailsReceived = () => ({
  type: SET_PENDING_DELETE_ALL_RECEIVED_MSG
})

const setSuccessDeletingAllMailsReceived = () => ({
  type: SET_SUCCESS_DELETE_ALL_RECEIVED_MSG
})

const setInvalidateDeletingAllMailsReceived = () => ({
  type: SET_INVALIDATE_DELETE_ALL_RECEIVED_MSG
})
// SENDED
const setInvalidateFetchMailsSended = () => ({
  type: SET_INVALIDATE_FETCH_SENDED_MSG
})

const setSuccessFetchMailsSended = (messages) => ({
  type: SET_SUCCESS_FETCH_SENDED_MSG,
  messages
})

const setPendingFetchMailsSended = () => ({
  type: SET_PENDING_FETCH_SENDED_MSG
})

const setPendingSendingMessage = () => ({
  type: SET_PENDING_SEND_MSG
})

const setSuccesMailSended = () => ({
  type: SET_SUCCESS_SEND_MSG
})

const setInvalidateMailSending = () => ({
  type: SET_INVALIDATE_SEND_MSG
})
// Delete One
const setPendingDeletingMailSended = () => ({
  type: SET_PENDING_DELETE_SENDED_MSG
})

const setInvalidateDeletingMailSended = () => ({
  type: SET_INVALIDATE_DELETE_SENDED_MSG
})

const setSuccessDeletingMailSended = () => ({
  type: SET_SUCCESS_DELETE_SENDED_MSG
})
// Delete All
const setPendingDeletingAllMailSended = () => ({
  type: SET_PENDING_DELETE_ALL_SENDED_MSG
})

const setInvalidateDeletingAllMailSended = () => ({
  type: SET_INVALIDATE_DELETE_ALL_SENDED_MSG
})

const setSuccessDeletingAllMailSended = () => ({
  type: SET_SUCCESS_DELETE_ALL_SENDED_MSG
})
// UNREAD
const setPendingFetchMessageUnreadCounter = () => ({
  type: SET_PENDING_MSG_UNREAD_COUNTER
})

const setSuccessFetchMessageUnreadCounter = (msgCounter) => ({
  type: SET_SUCCESS_MSG_UNREAD_COUNTER,
  msgCounter
})

const setInvalidateFetchMessageUnreadCounter = () => ({
  type: SET_INVALIDATE_MSG_UNREAD_COUNTER
})

const setSuccessFetchMessageUnread = (msg) => ({
  type: SET_SUCCESS_MSG_UNREAD,
  msg
})

const setInvalidateFetchMessageUnread = () => ({
  type: SET_INVALIDATE_MSG_UNREAD
})

const setPendingFetchMessageUnread = () => ({
  type: SET_PENDING_MSG_UNREAD
})

const setPendingEditMessageSaved = () => ({
  type: SET_PENDING_EDIT_MSG_SAVED
})

const setSuccessEditMessageSaved = () => ({
  type: SET_SUCCESS_EDIT_MSG_SAVED
})

const setInvalidateEditMessageSaved = () => ({
  type: SET_INVALIDATE_EDIT_MSG_SAVED
})
// Autres

export const setContentMsgToSend = (content) => ({
  type: SET_CONTENT_MSG_TO_SEND,
  content
})

const setSuccessFetchMessageSendedCounter = (msgCounter) => ({
  type: SET_SUCCESS_MSG_SENDED_COUNTER,
  msgCounter
})

const startMessageSavedCounter = (msgCounter) => ({
  type: SET_SUCCESS_MSG_SAVED_COUNTER,
  msgCounter
})

const setSuccessFetchMessageCounter = (msgCounter) => ({
  type: SET_SUCCESS_MSG_RECEIVED_COUNTER,
  msgCounter
})

const setPendingFetchMessageCounter = (msgCounter) => ({
  type: SET_PENDING_MSG_RECEIVED_COUNTER,
  msgCounter
})

export const startSendingMessage = (header, textBody, userId) => async (dispatch) => {
  try {
    dispatch(setPendingSendingMessage())

    const bodyRequest = { header, textBody }

    const response = await axios.post(`/mails/${userId}`, bodyRequest)

    dispatch(setSuccesMailSended())
    dispatch(setMsgSendedFocused(response.data))

    toast.success('💌 Votre message a été envoyé !', mailNotificationSetUp)

  } catch (err) {
    dispatch(setInvalidateMailSending())
    toast.error('📫 Impossible d\'envoyer le message.', mailNotificationSetUp)
  }
}

export const startFetchMessageReceived = () => async (dispatch) => {
  try {
    dispatch(setPendingFetchMailsReceiving())

    const response = await axios.get('/mails/received?sortBy=createdAt:desc')

    dispatch(setSuccessFetchMailsReceived(response.data))

  } catch (err) {

    dispatch(setInvalidateFetchMailsReceived())
  }
}

// Sécurité cotés backend, un utilisateur ne peut supprimer que SES msg !
export const startDeleteMessageReceived = (idMessage) => async (dispatch) => {
  try {
    dispatch(setPendingDeletingMailReceived())

    const response = await axios.delete(`/mails/received/${idMessage}`)

    dispatch(setSuccessDeletingMailReceived())
    dispatch(setMsgReceivedDeletedFocused(response.data))
    toast.success('💌 Votre message a été supprimé', mailNotificationSetUp)
  } catch (err) {
    dispatch(setInvalidateDeletingMailReceived())
    toast.error(`📫 Impossible de supprimer le message`, mailNotificationSetUp)
  }
}


export const startDeleteAllMessageReceived = () => async (dispatch) => {
  try {
    dispatch(setPendingDeletingAllMailsReceived())

    await axios.delete(`/mails/received/all`)

    dispatch(setSuccessDeletingAllMailsReceived())
    toast.success('💌 Tous les messages ont été supprimé', mailNotificationSetUp)
  } catch (err) {
    dispatch(setInvalidateDeletingAllMailsReceived())
    toast.error(`📫 Impossible de supprimer les messages`, mailNotificationSetUp)
  }
}


export const startSavingMessage = (header, textBody) => async (dispatch) => {
  try {
    dispatch(setPendingSavingMessage())

    const bodyRequest = {
      header,
      textBody
    }

    const response = await axios.post(`/mails/save`, bodyRequest)

    dispatch(setMsgSavedFocused(response.data))
    dispatch(setSuccesMailSaved())
    toast.success('💌 Votre message a été sauvegardé', mailNotificationSetUp)

  } catch (err) {
    dispatch(setInvalidateMailSaving())
    toast.error('📫 Impossible de sauvegarder le message.', mailNotificationSetUp)
  }
}

export const startFetchMessageSaved = () => async (dispatch) => {
  try {
    dispatch(setPendingFetchMailsSaved())

    const responses = await axios.get('/mails/saved')

    dispatch(setSuccessFetchMailsSaved(responses.data))

  } catch (err) {
    dispatch(setInvalidateFetchMailsSaved())
  }
}

export const startEditMessageSaved = () => async (dispatch, getState) => {
  try {
    dispatch(setPendingEditMessageSaved())

    const state = getState()
    const { _id, header, textBody } = state.filters.msgSavedFocused
    const bodyRequest = { header, textBody }

    const response = await axios.patch(`/mails/saved/${_id}`, bodyRequest)

    dispatch(setSuccessEditMessageSaved())
    dispatch(setMsgSavedFocused(response.data))
    toast.success('💌 Votre message a été édité', mailNotificationSetUp)

  } catch (err) {
    dispatch(setInvalidateEditMessageSaved())
    toast.error(`📫 Impossible d'éditer le message.`, mailNotificationSetUp)
  }
}

export const startDeleteMessageSaved = (idMessage) => async (dispatch) => {
  try {
    dispatch(setPendingDeletingMailSaved())

    const response = await axios.delete(`/mails/saved/${idMessage}`)

    dispatch(setSuccessDeletingMailSaved())
    dispatch(setMsgSavedDeletedFocused(response.data))
    toast.success('💌 Votre message a été supprimé', mailNotificationSetUp)

  } catch (err) {
    dispatch(setInvalidateDeletingMailSaved())
    toast.error(`📫 Impossible de supprimer le message`, mailNotificationSetUp)
  }
}

export const startDeleteAllMessageSaved = () => async (dispatch) => {
  try {
    dispatch(setPendingDeletingAllMailsSaved())

    await axios.delete(`/mails/saved/all`)

    dispatch(setSuccessDeletingAllMailsSaved())
    toast.success('💌 Tous les messages ont été supprimés', mailNotificationSetUp)

  } catch (err) {
    dispatch(setInvalidateDeletingAllMailsSaved())
    toast.error(`📫 Impossible de supprimer les messages`, mailNotificationSetUp)
  }
}


export const startFetchMessageSended = () => async (dispatch) => {
  try {
    dispatch(setPendingFetchMailsSended())

    const response = await axios.get('/mails/sended')
    dispatch(setSuccessFetchMailsSended(response.data))

  } catch (err) {
    dispatch(setInvalidateFetchMailsSended())
  }
}

export const startDeleteMessageSended = (idMessage) => async (dispatch) => {
  try {
    dispatch(setPendingDeletingMailSended())

    const response = await axios.delete(`/mails/sended/${idMessage}`)

    dispatch(setSuccessDeletingMailSended())
    dispatch(setMsgSendedDeletedFocused(response.data))
    toast.success('💌 Votre Message a été Supprimé', mailNotificationSetUp)
  } catch (err) {
    dispatch(setInvalidateDeletingMailSended())
    toast.error(`📫 Impossible de supprimer le message`, mailNotificationSetUp)
  }
}

export const startDeleteAllMessageSended = () => async (dispatch) => {
  try {
    dispatch(setPendingDeletingAllMailSended())

    await axios.delete(`/mails/sended/all`)

    dispatch(setSuccessDeletingAllMailSended())
    toast.success('💌 Tous les Messages ont été Supprimés', mailNotificationSetUp)

  } catch (err) {
    dispatch(setInvalidateDeletingAllMailSended())
    toast.error(`📫 Impossible de supprimer les messages`, mailNotificationSetUp)
  }
}

export const startFetchMessageCounter = () => async (dispatch) => {
  try {

    dispatch(setPendingFetchMessageCounter())

    const response = await axios.get('mails/received/counter')
    dispatch(setSuccessFetchMessageCounter(response.data.count))

  } catch (err) {
    console.log(err)
  }
}

export const startFetchMessageUnread = () => async (dispatch) => {
  try {
    dispatch(setPendingFetchMessageUnread())

    const response = await axios.get('mails/received/unread')

    dispatch(setSuccessFetchMessageUnread(response.data))

  } catch (err) {
    dispatch(setInvalidateFetchMessageUnread())
  }
}

export const startFetchMessageUnreadCounter = () => async (dispatch) => {
  try {
    dispatch(setPendingFetchMessageUnreadCounter())

    const response = await axios.get('/mails/unread')

    dispatch(setSuccessFetchMessageUnreadCounter(response.data.length))

  } catch (err) {
    dispatch(setInvalidateFetchMessageUnreadCounter())
  }
}

export const startFetchMessageSendedCounter = () => async (dispatch) => {
  try {

    const response = await axios.get('/mails/sended')

    dispatch(setSuccessFetchMessageSendedCounter(response.data.length))

  } catch (err) {
    console.log(err)
  }
}

export const startFetchMessageSavedCounter = () => async (dispatch) => {
  try {

    const response = await axios.get('/mails/saved')

    dispatch(startMessageSavedCounter(response.data.length))

  } catch (err) {
    console.log(err)
  }
}
