import axios from 'axios'
import { toast } from 'react-toastify'
export const SET_AVATAR_UPLOAD_PENDING = 'SET_AVATAR_UPLOAD_PENDING'
export const SET_AVATAR_UPLOAD_SUCCESS = 'SET_AVATAR_UPLOAD_SUCCESS'
export const SET_AVATAR_UPLOAD_ERROR = 'SET_AVATAR_UPLOAD_ERROR'
export const SET_AVATAR_ID = 'SET_AVATAR_ID'
export const SET_DELETE_DASHBOARD_PENDING = 'SET_DELETE_DASHBOARD_PENDING'
export const SET_DELETE_DASHBOARD_INFOS = 'SET_DELETE_DASHBOARD_INFOS'
export const SET_DELETE_DASHBOARD_INVALIDATE = 'SET_DELETE_DASHBOARD_INVALIDATE'

export const dashboardNotificationSetUp = {
  position: 'bottom-right',
  autoClose: 3000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true
}

const setAvatarUploadPending = () => ({
  type: SET_AVATAR_UPLOAD_PENDING
})

const setAvatarUploadSuccess = () => ({
  type: SET_AVATAR_UPLOAD_SUCCESS
})

const setAvatarUploadError = (errorMessage) => ({
  type: SET_AVATAR_UPLOAD_ERROR,
  errorMessage
})

export const setAvatarId = (avatarId) => ({
  type: SET_AVATAR_ID,
  avatarId
})

export const setDeleteDashboardPending = () => ({
  type: SET_DELETE_DASHBOARD_PENDING
})

export const setDeleteDashboardInfos = () => ({
  type: SET_DELETE_DASHBOARD_INFOS
})

export const setDeleteDashboardInvalidate = () => ({
  type: SET_DELETE_DASHBOARD_INVALIDATE
})

export const startUploadAvatar = (avatarFile) => async (dispatch, getState) => {
  try {
    const state = getState()
    const token = state.user.token
    dispatch(setAvatarUploadPending())

    const data  = new FormData()
    data.append('avatar', avatarFile)

    const response = await axios.post(`${process.env.MONGODB_DATABASE_URL}/users/me/avatar`, data, {
      headers: { Authorization: `Bearer ${token}` }
    })

    dispatch(setAvatarUploadSuccess())

    if(!state.dashboard.avatarId)
      {
        dispatch(setAvatarId(response.data.idAvatar))
      }

    toast.success('🐵 Votre image profil a été changée !', dashboardNotificationSetUp)

  } catch (err) {
    dispatch(setAvatarUploadError('inconnue'))

    if (err.response.data.error === 'File too large') {
        dispatch(setAvatarUploadError('image trop lourde'))
        return toast.error('🐘 Image trop lourde ! 5 mo Maximum.', dashboardNotificationSetUp)
    }

    toast.error('🏴‍☠️ Une Erreur s\'est produite', dashboardNotificationSetUp)
  }
}

export const startDeleteDashboardInfos = () => async (dispatch) => {
  try {
    dispatch(setDeleteDashboardPending())
    dispatch(setDeleteDashboardInfos())
  } catch (err) {
    dispatch(setDeleteDashboardInvalidate())
  }
}
