import axios from 'axios'
import { toast } from 'react-toastify'
import { setAvatarId } from './users/dashboardAction'
import { setUserInfo, setIsAuthenticated, setIsAdmin } from '../actions/loginActions'
export const SET_REQUEST_ACCOUNT_CREATION = 'SET_REQUEST_ACCOUNT_CREATION'
export const SET_INFOS_ACCOUNT_CREATION = 'SET_INFOS_ACCOUNT_CREATION_FAILED'
export const SET_ACCOUNT_CREATION_FAILED = 'SET_ACCOUNT_CREATION_FAILED'
export const SET_ACCOUNT_CREATION_SUCCEED = 'SET_ACCOUNT_CREATION_SUCCEED'

export const createUserNotificationSetUp = {
  position: 'bottom-right',
  autoClose: 3000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true

}

export const saveInfosWhenTryingToCreateAccount = (email, password, pseudoName) => ({
  type: SET_INFOS_ACCOUNT_CREATION,
  email,
  password,
  pseudoName
})

const setRequestAccountCreation  = () => ({
  type: SET_REQUEST_ACCOUNT_CREATION
})

const setAccountCreationSuccess  = () => ({
  type: SET_ACCOUNT_CREATION_SUCCEED,

})

const setAccountCreationFailed  = () => ({
  type: SET_ACCOUNT_CREATION_FAILED
})


export const startAccountCreation = (email, password, pseudoName) => async (dispatch) => {
  try {
    dispatch(saveInfosWhenTryingToCreateAccount(email,password,pseudoName))
    dispatch(setRequestAccountCreation())

    const bodyRequest = {
      email,
      password,
      pseudoName
    }
    const response = await axios.post(`${process.env.MONGODB_DATABASE_URL}/users`, bodyRequest)
    toast.success('💎 Votre compte a été créer !', createUserNotificationSetUp)

    dispatch(setUserInfo(response.data.token, response.data.user))
    dispatch(setAccountCreationSuccess())
    dispatch(setIsAuthenticated())
    dispatch(setAvatarId(response.data.user.avatarId || null))

    if (response.data.user.roleName === 'admin') {
      dispatch(setIsAdmin(true))
    }

  } catch (err) {
    dispatch(setAccountCreationFailed())
    if (err.response.data.code === 11000) {
        return toast.error('👯 Cette adresse email existe déjà !', createUserNotificationSetUp)
    }
    toast.error('💥 OOPS Une erreur s\'est produite.', createUserNotificationSetUp)
  }
}
