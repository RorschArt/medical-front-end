import axios from 'axios'
export const SET_INVALIDATE_ARTICLES = 'SET_INVALIDATE_ARTICLES'
export const SET_INFOS_ARTICLES_RECEIVED = 'SET_INFOS_ARTICLES_RECEIVED'
export const SET_REQUEST_ARTICLES = 'SET_REQUEST_ARTICLES'

const setArticlesReceivedInfo = (articles) => ({
  type: SET_INFOS_ARTICLES_RECEIVED,
  articles,
  lastUpdate: Date.now()
})

const setInvalidateArticles = () => ({
  type: SET_INVALIDATE_ARTICLES
})

const setRequestArticles = () => ({
  type: SET_REQUEST_ARTICLES
})


export const getAllArticles = () => async (dispatch) => {
  try {
    dispatch(setRequestArticles())

    const articles = await axios.get(`${process.env.MONGODB_DATABASE_URL}/articles`)

    dispatch(setArticlesReceivedInfo(articles.data))
  } catch (e) {
    dispatch(setInvalidateArticles())
  }
}

export const shouldFetchArticles = (state) => {
  if (state.articles.items.length === 0) {
    return true
  }
  if (state.articles.isPending) {
    return false
  }
  return state.articles.didInvalidate
}

export const fetchArticlesIfNeeded = () => (dispatch, getState) => {
  const state = getState()

  if (shouldFetchArticles(state)) {
    return dispatch(getAllArticles())
  }
}


export const getArticle = (idArticle) => async (dispatch) => {
  try {

    const article = await axios.get(`${process.env.MONGODB_DATABASE_URL}/article/${idArticle}`)

    dispatch(setArticleInfo(article.data))
  } catch (err) {
    console.log(err)
  }
}
