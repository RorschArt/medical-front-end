import articlesReducer from '../../reducers/articlesReducer'
import articlesList from '../fixtures/articles'
import moment from '../__mocks__/moment'

test('Devrait permettre de donner un state par default au début de l\'application', () => {
  const state = articlesReducer(undefined, { type: '@@INIT' })
  expect(state).toEqual({
    items: [],
    isPending: false,
    didInvalidate: false,
    lastUpdate: null,
    error: false
  })
})

test('Devrait changer le state correctement après un fetch réussi', () => {
  const action = {
    type: 'SET_INFOS_ARTICLES_RECEIVED',
    articles: articlesList,
    lastUpdate: moment()
  }

  const state = articlesReducer(undefined, action)

  expect(state).toEqual({
    items: articlesList,
    isPending: false,
    lastUpdate: moment(),
    didInvalidate: false,
    error: false
  })
})

test('Devrait changer le status \'isPending\' lors d\'un fetch des articles', () => {
  const action = {
    type: 'SET_REQUEST_ARTICLES'
  }

  const state = articlesReducer(undefined, action)

  expect(state). toEqual({
    items:[],
    lastUpdate: null,
    isPending: true,
    didInvalidate: false,
    error: false
  })
})

test('Ne Devrait pas permettre d\'avoir des articles duppliqués après un fetch d\'articles.', () => {
  const action = {
    type: 'SET_INFOS_ARTICLES_RECEIVED',
    articles: articlesList,
    lastUpdate: moment()
  }

  const firstState  = articlesReducer(undefined, action)
  const secondState = articlesReducer(firstState, action)

  expect(secondState).toEqual({
    items: articlesList,
    isPending: false,
    lastUpdate: moment(),
    didInvalidate: false,
    error: false
  })
})

test('Devrait mettre \'didInvalidate\' a true lors d\'un echec de fetch des articles',() => {
  const action = {
    type: 'SET_INVALIDATE_ARTICLES',
  }

  const state = articlesReducer(undefined, action)
  expect(state).toEqual({
    items: [],
    isPending: false,
    lastUpdate: null,
    didInvalidate: true,
    error: 'INVALIDATE_ARTICLES'
  })
} )
