export default [
      {
        title: 'Titre 1',
        subTitle: 'Sous Titre 1',
        textBodyArticle: 'Body Texte 1',
        _id: '5d2daa5d4bd3500017553277',
        imageUrl: 'Mon urlu3',
        author: {
          _id: '5d2daa404bd3500017553175',
          roleName: 'admin',
          user: {
            pseudoName: 'admin',
            age: 782,
            _id: '5d2daa404bd3500017553274',
            email: 'lop@hotmail.fr',
            createdAt: '2019-07-16T10:43:12.863Z',
            updatedAt: '2019-07-16T10:43:12.863Z',
            __v: 0
          },
          __v: 0
        },
        createdAt: '2019-07-16T10:43:41.833Z',
        updatedAt: '2019-08-12T15:35:33.810Z',
        __v: 0
      },
      {
        title: 'Titre 2',
        subTitle: 'Sous Titre 2',
        textBodyArticle: 'Body Texte 2',
        _id: '5d4ac37b13a5d50017791470',
        author: {
          _id: '5d4ac37013a5d5001779146e',
          roleName: 'admin',
          user: {
            pseudoName: 'admin',
            age: 782,
            _id: '5d4ac37013a5d5001779146d',
            email: 'nopeeee@hotmail.fr',
            createdAt: '2019-08-07T12:26:24.254Z',
            updatedAt: '2019-08-12T11:16:47.082Z',
            __v: 30
          },
          __v: 0
        },
        createdAt: '2019-08-07T12:26:35.962Z',
        updatedAt: '2019-08-12T16:11:23.600Z',
        __v: 0
      },
      {
        title: 'Titre 3',
        subTitle: 'Sous Titre 3',
        textBodyArticle: 'Body text 3',
        _id: '5d5945f37025180017e9e184',
        author: {
          _id: '5d514c085b982c0017fa95cd',
          roleName: 'admin',
          user: {
            pseudoName: 'admin',
            age: 782,
            _id: '5d514c085b982c0017fa95cc',
            email: 'admin4@gmail.com',
            createdAt: '2019-08-12T11:22:48.441Z',
            updatedAt: '2019-08-18T12:35:56.355Z',
            __v: 101
          },
          __v: 0
        },
        createdAt: '2019-08-18T12:34:59.761Z',
        updatedAt: '2019-08-18T12:34:59.761Z',
        __v: 0
      }
    ]
