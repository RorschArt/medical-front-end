const webpack = require('webpack')
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

process.env.NODE_ENV = process.env.NODE_ENV || 'development'

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({ path: '.env.development' })
}

module.exports = (env) => {
  // Determine si on utilise le script en prod pour changer les source-maps
  const isProdMode = env === 'production'
  const CSSExtract = new MiniCssExtractPlugin({ filename: 'styles.css' })

  return {
    mode: 'development',
    entry: ['babel-polyfill', './src/app.js'],
    output: {
      path: path.join(__dirname, 'public', 'dist'),
      filename: 'bundle.js'
    },
    module: {
      rules: [
        {
          loader: 'babel-loader',
          // Sur quel fichiers on souhaites cette règle.
          test: /\.js$/,
          // Les fichiers ou dossiers exclus.
          exclude: /node_modules/
        },
        {
          test: /\.s?css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        }
      ]
    },
    plugins: [
      CSSExtract,
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new webpack.DefinePlugin({
        'process.env': {
          'MONGODB_DATABASE_URL': JSON.stringify(process.env.MONGODB_DATABASE_URL)
        }
      })
    ],
    devtool: isProdMode ? 'source-map' : 'inline-source-map',
    devServer: {
      contentBase: path.join(__dirname, 'public'),
      // dit à webpack que l'on est sur une webapp client-side, sans ça
      // le html n'est pas renvoyé lors des requetes url car le navigateur
      // interroge le server.
      historyApiFallback: true,
      publicPath: '/dist/'
    }
  }
}
