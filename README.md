# Medical-front-end

Front-End de mon projet

## Scripts 💻

`dev-server`: Demarrer le serveur webpack (babel)  
`test`: Demarrer les tests (jest /enzyme)  
`build:dev`: Transpiler (babel) la totalité du projet (Supprimer le dossier  
  `public/dist` au préalable)  

## Les URL 📡

[Projet Back End](https://gitlab.com/AxelDaguerre/medical-back-end)

[Site production](https://medical-front-end.herokuapp.com)

[Slide de mon projet](https://slides.com/axelaguerred/medical-project)

[Rapport de stage](https://gitlab.com/AxelDaguerre/my-documentation/blob/master/Stage/Project.md)